*******************************************
*																					*
*  ISTRUZIONI PER L'USO DEL SISTEMA BRIS	*
*																					*
*******************************************


Indice:

	1 - Requisiti
	2 - Installazione brsserver
	3 - Installazione brsclient
	4 - Esecuzione brsserver
	5 - Esecuzione brsclient


----------------------------------------------------------------------
1 - Requisiti
----------------------------------------------------------------------

Per poter effettuare l'installazione e l'esecuzione del server e del
client, sono necessari i seguenti componenti:

	- gcc
	- make


----------------------------------------------------------------------
2 - Installazione brsserver
----------------------------------------------------------------------

Per eseguire l'installazione del server, occorre aprire un terminale
e posizionarsi nella cartella "src".

Sempre da terminale lanciare il comando:

	make install-server

Il comando creerà l'eseguibile "brsserver" e mostrerà sul terminale
tutte le operazioni che vengono effettuate durante la compilazione.


----------------------------------------------------------------------
3 - Installazione brsclient
----------------------------------------------------------------------

Per eseguire l'installazione del client, occorre aprire un terminale
e posizionarsi nella cartellla "src".

Sempre da terminale lanciare il comando:

	make install-client

Il comando crerà l'eseguibile "brsclient" e mostrerà sul terminale
tutte le operazioni che vengono effettuate durante la compilazione.


----------------------------------------------------------------------
4 - Esecuzione brsserver
----------------------------------------------------------------------

Per lanciare il server brsserver, occorre aprire un terminale e
posizionarsi nella cartella dove è presente l'eseguibile brsserver.

Prima di eseguire il server, è consigliato digitare il comando:

	make clean_server
	
Esso infatti rimuove i file di log relativi alle precedenti partite,
i file temporanei e ripulisce l'ambiente.

A questo punto, per eseguire il server, occorre digitare il comando:

	./brsserver <file_utenti>

dove "file_utenti" è un file contenente l'elenco degli utenti e delle
loro password (salvate in chiaro) che sono ammessi all'utilizzo del
server secondo il formato "nome_utente:password".

Una volta lanciato il comando verrà visualizzata la stringa
"*** Server Pronto ***" nel caso in cui non si siano verificati errori,
altrimenti una stringa con la descrizione dell'errore.

Se si vuole avviare il server in modalità di testing, ovvero i mazzi
saranno generati in modo non casuale, occorre inserire l'opzione
"-t" dopo il "file_utenti" all'avvio del server, in questa maniera:

	./brsserver <file_utenti> -t


----------------------------------------------------------------------
5 - Esecuzione brsclient
----------------------------------------------------------------------

Per lanciare il client brsclient, occorre aprire un terminale e
posizionarsi nella vartella dove è presente l'eseguibile brsclient.


A questo punto, si può attivare il client in tremodi diversi, a
seconda di ciò che dobbiamo fare.


1) Se si desidera effettuare una registrazione di un utente
all'albero degli utenti, occorre lanciare il client con il comando:

	./brsclient <nome_utente> <password> -r

dove "nome_utente" è il nome dell'utente da registrare, e "password" è
la password che si desidera associare all'utente.
Successivamente verrà visualizzato sul terminale l'esito
dell'operazione.


2) Se invece si desidera effettuare la cancellazione di un utente
dall'albero degli utenti, occorre lanciare il client con il comando:

	./brsclient <nome_utente> <password> -c

dove "nome_utente" è il nome dell'utente da registrare, e "password" è
la password che si desidera associare all'utente.
Successivamente verrà visualizzato sul terminale l'esito
dell'operazione.


3) Se invece si desidera effettuare una partita, occorre lanciare il
client con il comando:

	./brsclient <nome_utente> <password>

dove "nome_utente" è il nome dell'utente (esso deve essere presente
nell'albero degli utenti per poter effettuare una partita) e
<password> è la sua password.
Verranno visualizzate sul terminale le istruzioni da eseguire per
cominciare una partita con un altro giocatore connesso.
