/**
   \file brsservcli.h
   \author Luca Di Mauro.
   \brief Header libreria tipo lista e macro server e client progetto bris.

		Si dichiara che il contenuto di questo file e' in ogni sua parte opera
		originale dell' autore.
 */

#ifndef _BRSSERVCLI_H
#define _BRSSERVCLI_H

#include <pthread.h>

/* ***** TIPO LISTA ***** */

/** La struttura ::element rappresenta un ::thread_worker, attivo o meno, appartenente al server.
	* - \c PID contiene il thread identifier del thread cui è associato l'elemento della lista.
	* - \c FD contiene il file descriptor della connessione con il client.
	* - \c detached è un semaforo che serve a testare se un thread ha chiamato o meneo la pthread_detach.
	* - \c next è un puntatore al successivo elemento della lista.
*/	
typedef struct element
{
	/** Thread identifier. */
	pthread_t PID;
	/** File descriptor della connessione. */
	int FD;
	/** Semaforo sul quale chiamare la pthread_try_lock. */
	pthread_mutex_t detached;
	/** Puntatore al prossimo elemento. */
	struct element *next;
} element_t;


/** Path della socket del progetto bris. */
#define BRSSOCKET "./tmp/briscola.skt"
/** Nome del file sul quale registrare l'albero degli utenti alla ricezione del segnale SIGUSR1. */
#define CHECKPOINT "brs.checkpoint"
/** Nome del file sul quale redirigere lo standard error del server. */
#define FILE_ERROR "./err_server"

/** Numero di volte da riprovare per la creazione del thread worker. */
#define N_TRY_THREAD 10
/** Numero di secondi da aspettare prima di riprovare la creazione del thread worker. */
#define SEC_TRY_TREAD 2
/** Numeri di turni che fa un giocatore. */
#define N_TURNI 20
/** Punteggio massimo che può totalizzare un giocatore. */
#define MAX_SCORE 120



/*__Stringhe per i messaggi di risposta al client.__*/

/** L'utente è già presente nell'albero. */
#define ALREADYEXIST "Utente già presente"
/** L'utente non è presente nell'albero. */
#define NOUSER "Utente non presente"
/** La password associata all'utente non corrisponde con quella inserita. */
#define WRONGPWD "Password errata"
/** L'utente inserito non è disponibile per una partita poiché non è più in stato WAIT. */
#define USERUNAVAILABLE "L'utente non è più disponibile per una partita"
/** L'utente è già connesso al servizio (in stato WAIT o PLAYING). */
#define ALREADYCONNECT "L'utente è già connesso"


#endif 
