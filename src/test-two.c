/**
   \file
   \author lso13
   
   \brief Test primo frammento
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>

#include "users.h"


/** nome file di test */
#define FILETEST1 "./usergroup.txt"


/** esempi di utenti ben formati */
char* goodUsers[] = {
  "paperino:Cn4v5jK",
  "pippo:0100ddd",
  "minni:kkklll9",
  "quiquoqua:ll,,..,:",
  "aaaaaaaaaaaaaaaaaaaa:bbbbbbbb",
  NULL,
};

/** esempi di utenti errati */
char* badUsers[] = {
  "paperino:Cn4v5jKaaa", /* pwd troppo lunga */
  "pippo:", /* pwd assente */
  ":kkklll9", /* user assente */
  "aaaaaaaaaaaaaaaaaaaaAAA:bbbbbbbb", /* user troppo lungo */
  "minnie",/* manca :*/
  NULL,
};

/** test utenti da rimuovere */
char* rmUsers[] = {
  "quiquoqua:ll,,..,:",
  "pippo:kkk", /* passwd errata*/
  "pippo:0100ddd",
  "paperino:Cn4v5jK",
  "aaaaaaaaaaaaaaaaaaaa:bbbbbbbb",
  "paperino:bbbbbbbb",/* passwd errata e utente gia' rimosso*/
  "aaaaaaaaaaaaaaaaaaaa:bbbbbbbb",/* utente gia rimosso*/
  "minni:kkklll9",
  NULL,
};

/** risposte utenti da rimuovere */
int answRmUsers[] = {
  0, /*"quiquoqua:ll,,..,:" */
  WRPWD,      /*"pippo:kkk" passwd errata*/
  0, /*"pippo:0100ddd"*/
  0, /*"paperino:Cn4v5jK"*/
  0, /*"aaaaaaaaaaaaaaaaaaaa:bbbbbbbb"*/
  NOUSR,      /*"paperino:bbbbbbbb" passwd errata ma utente gia' rimosso*/
  NOUSR,      /*"aaaaaaaaaaaaaaaaaaaa:bbbbbbbb" utente gia rimosso*/
  0, /*"minnie:kkklll9"*/
};

/** main di test */
int main (void) {
  int i,cont;
  user_t* us;
  char *r;
  nodo_t* root=NULL;
  FILE* fsa;

  /* controllo memoria */
  mtrace();

  /* test conversione utenti ben formattati */
  for(i=0;goodUsers[i]!=NULL;i++) {
    fprintf(stderr,"converto--%s\n",goodUsers[i]);
    if ( (us = stringToUser(goodUsers[i],strlen(goodUsers[i])+1)) == NULL ) {
      perror("stringToUser: Errore fatale");
      exit(EXIT_FAILURE);
    }

    if ( ( ( r = userToString(us) ) == NULL ) || ( strcmp(goodUsers[i],r) != 0 ) )  {
      perror("userToString: Errore fatale");
      free(us);
      exit(EXIT_FAILURE);
    }
    fputs(r,stdout);
    fputc('\n',stdout);

    free(r);
    free(us);
  }

  /* quanti utenti ho convertito */
  cont = i;

 /* test conversione utenti mal formati */
  for(i=0;badUsers[i]!=NULL;i++) {
    fprintf(stdout,"converto--%s\n",badUsers[i]);
    if ( (us = stringToUser(badUsers[i],strlen(badUsers[i])+1)) != NULL ) {
      perror("stringToUser 2: Errore fatale");
      exit(EXIT_FAILURE);
    }
  }


  fprintf(stdout,"Test Albero Utenti.\n");

  /* test creazione albero */
  for(i=0;goodUsers[i]!=NULL;i++) {
    user_t * p;
    
    if ( (us = stringToUser(goodUsers[i],strlen(goodUsers[i])+1)) == NULL ) {
      perror("stringToUser 3: Errore fatale");
      exit(EXIT_FAILURE);
    }

    
    if ( addUser(&root,us) != 0 ) {
      perror("addUser: Errore fatale");
      free(us);
      exit(EXIT_FAILURE);
    }
    

    if ( (p = stringToUser(goodUsers[i],strlen(goodUsers[i])+1)) == NULL ) {
       perror("stringToUser 4: Errore fatale");
      exit(EXIT_FAILURE);
    }

    if (  checkPwd(root,p)  == FALSE ){
      perror("checkPwd: Errore fatale");
      free(p);
      exit(EXIT_FAILURE);
    }
    free(p);


  }
  
  /* stampa albero su stdout */
  fprintf(stdout,"Stampa Albero Utenti.\n");
  printTree(root);
  fprintf(stdout,"Fine Stampa Albero Utenti.\n");

  /* registrazione utenti su file */
  fprintf(stdout,"Registro gli utenti su file.\n");
  fsa=fopen(FILETEST1,"w");
  if ( ( fsa == NULL ) || ( cont != storeUsers(fsa,root) ) ) {
    perror("storeUsers: Errore fatale");
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Registrati %d utenti.\n",cont);
  fclose(fsa);
  

 /* test rimozione utenti */
  for(i=0;rmUsers[i]!=NULL;i++) {

    
    if ( (us = stringToUser(rmUsers[i],strlen(rmUsers[i])+1)) == NULL ) {
      perror("stringToUser 5: Errore fatale");
      exit(EXIT_FAILURE);
    }

    
    if ( removeUser(&root,us) != answRmUsers[i] ) {
      perror("removeUser: Errore fatale");
      free(us);
      exit(EXIT_FAILURE);
    }
    free(us);
  }
  /* stampa albero su stdout */
  printTree(root); 

  /* test du loadUsers */
  fprintf(stdout,"Rileggo gli utenti da file.\n");
  fsa=fopen(FILETEST1,"r");
  if ( cont != loadUsers(fsa,&root) ) {
    perror("loadUsers: Errore fatale");
    exit(EXIT_FAILURE);
  }
  
  fprintf(stdout,"Letti %d permessi.\n",cont);
  fclose(fsa);

  /* cleanup */
  freeTree(root);
  muntrace();
  fprintf(stdout,"Test Superato!!\n");
  return EXIT_SUCCESS; 
}

