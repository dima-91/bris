/**
   \file brsserver.c
   \author Luca Di Mauro.
   \brief Codice server progetto bris.

		Si dichiara che il contenuto di questo file e' in ogni sua parte opera
		originale dell' autore.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "comsock.h"
#include "bris.h"
#include "users.h"
#include "newMazzo_r.h"
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "brsservcli.h"



/* *** VARIABILI GLOBALI *** */

/** \brief Puntatore all'albero binario di ricerca degli utenti. */
nodo_t *AlberoUtenti=NULL;
/**	\brief Variabile che indica la possibilità di accettare connessioni. */
volatile sig_atomic_t accept_connection=1;
/**	\brief ID del ::thread_disp. */
pthread_t TID;
/** \brief Variabile per indicare la modalità di testing del server. */
bool_t test= FALSE;
/** \brief Variabile che identifia il numero della partita. */
volatile sig_atomic_t n_partita=1;
/** \brief Lista per memorizzare gli ID e i mutex dei ::thread_worker. */
element_t *List=NULL;



/* *** SEMAFORI E VARIABILI DI CONDIZIONE *** */

/** \brief Semaforo per l'accesso all'::AlberoUtenti. */
pthread_mutex_t mux_tree= PTHREAD_MUTEX_INITIALIZER;
/**	\brief Variabile di condzione per l'accesso all'::AlberoUtenti. */
pthread_cond_t cond_tree= PTHREAD_COND_INITIALIZER;
/**	\brief Variabile per indicare che un thread sta eseguendo operazioni sull'::AlberoUtenti. */
bool_t	workOnTree= FALSE;



/* *** DEFINIZIONI DI FUNZIONI *** */

/** \brief Funzione per gestitre l'arrivo del segnale SIGUSR1. */
void gestore_USR1 ();
/** \brief Funzione per gestre l'arrivo dei segnali SIGINT e SIGTERM. */
void gestore_TERM_INT ();
/** \brief Funzione per gestire il segnale SIGUSR2 che il ::gestore_TERM_INT manda al ::thread_worker. */
void gestore_USR2 ();
/**	\brief Funzione da assegnare ai thread che interagiscono con i client.
    \see brsclient.c
*/
void *thread_worker (void *arg);
/**	\brief Funzione da assegnare al thread che gestisce le richieste di connessione. */
void *thread_disp (void *arg);



/**
  * Con questa funzione gestisco il segnale SIGUSR1, che mi richiede una stampa dell'::AlberoUtenti su file.
  * Apro quindi il file in scrittura, eseguo una ::storeUsers e chiudo il file.
  * Se qualcosa va male termino il server.
*/
void gestore_USR1 ()
{
  FILE *fp_check=NULL;

	if ((fp_check= fopen (CHECKPOINT, "w+"))==NULL)
	{
		perror ("server (gestore_USR1) crashed on fopen");
		exit (EXIT_FAILURE);
	}

  if ((storeUsers (fp_check, AlberoUtenti))==-1)
	{
		perror ("server (gestore_USR1) crashed on storeUsers");
		exit (EXIT_FAILURE);
	}

	if ((fclose (fp_check))!=0)
	{
		perror ("server (gestore_USR1) crashed on fclose");
		exit (EXIT_FAILURE);
	}
}



/**
	* Lo scopo di questa funzione è settare la variabile globale ::accept_connection che
	* indica la possibilitià o meno di accettare nuove connessioni, e inviare al ::thread_disp
	* il segnale SIGUSR2 in modo che la ::acceptConnection si sblocchi all'arrivo del segnale
	* e il ::thread_disp termini la sua esecuzione normalmente.
*/
void gestore_TERM_INT ()
{
	int err;
	accept_connection=0;
	if ((err=pthread_kill (TID, SIGUSR2))!=0)
	{
		errno=err;
		perror ("server (gestore_TERM_INT) crashed on pthread_kill");
		exit (EXIT_FAILURE);
	}

	return ;
}





/**
	* Questa funzione non fa niente. Serve solo a gestire
	* il segnale SIGUSR2 che la funzione ::gestore_TERM_INT invia al ::thread_disp.
*/
void gestore_USR2 ()
{
	return ;
}





/**
	* Funzione main del programma.
	* La funzione carica gli utenti presenti sul file (passato come primo argomento) nell'::AlberoUtenti.
	* Successivamente installa i gestori dei segnali, apre la socket ::BRSSOCKET, e crea il ::thread_disp,
  * che si mette in ascolto sulla socket per nuove connessioni, e ne attede la terminazione.
	* Dopo attende la terminazione di tutti i ::thread_worker, in modo che terminino le partite
	* già iniziate, salva l'::AlberoUtenti su file, chiude la scoket e termina.
*/
int main (int argn, char **arg)
{
	/* Socket sulla quale si connetteranno i client. */
	int socket=0;
	/* Puntatore al file degli utenti. */
	FILE *fp=NULL;
	/* Puntatore di supporto per la lista dei thread. */
	element_t *app=NULL;
	/* Varriabile per salvare il valore di errno nelle varie situazioni di errore. */
	int err=0;
	/* Variabile per salvare l'errore della pthread_mutex_trylock. */
	int etry=0;
	/* Intero per salvare lo stato di terminazione dei thread. */
	int *retval= malloc (sizeof (int));
	/* Struttura per la gestione del segnale	USR1. */
	struct sigaction sa_USR1;
	/* Struttura per la gestione dei segnale	SIGTERM e SIGINT. */
	struct sigaction sa_TERM_INT;
  /* Struttura per la gestione del segnale SIGPIPE. */
  struct sigaction sa_PIPE;
	/* Maschera di bit per bloccare i segnali. */
	sigset_t mask;
	/* File descrpitor del file sul quale redirigo lo standard error. */
	int p_err=0;

	*retval=0;

	/* Elimino il file err_server se esiste. Creo il file err_server, lo apro in scrittura
		 e redirigo lo standard error su di esso.
	*/
	if ((unlink (FILE_ERROR))==-1)
	{
		if (errno!=ENOENT)
		{
			perror ("server (main) crashed on unlink err_server");
			exit (EXIT_FAILURE);
		}
	}

	if ((p_err= open (FILE_ERROR, O_WRONLY | O_TRUNC | O_CREAT, 0666))==-1)
	{
		perror ("server (main) crashed on creat/open err_server");
		close (p_err);
		exit (EXIT_FAILURE);
	}

	/* Redeirigo lo standard error su un file per evitare di visualizzare messaggi di errore.
	*/
	if ((dup2 (p_err, 2))==-1)
	{
		perror ("server (main) crashed on dup2");
		close (p_err);
		exit (EXIT_FAILURE);
	}

	if ((close (p_err))==-1)
	{
		perror ("server (main) crashed on close err_serv");
		exit (EXIT_FAILURE);
	}

	/*	Controllo gli argomenti.
	*/
	if (argn == 3 && (strcmp (arg[2], "-t"))==0)
		test= TRUE;
	else if (argn!=2)
	{
		errno= EINVAL;
		perror ("server (main) crashed on check arguments");
		exit (EXIT_FAILURE);
	}

	/* Maschero tutti i segnali che possono arrivare al processo.
	*/
	if ((sigfillset (&mask))!=0)
	{
		perror ("server (main) crashed on sigfillset 0");
		exit (EXIT_FAILURE);
	}

	/* Apro il file in lettura e carico gli utenti nell'albero.
	*/
	if ((fp= fopen (arg[1], "r"))==NULL)
	{
		perror ("server (main) crashed on fopen 0 arg[1]");
		exit (EXIT_FAILURE);
	}

	/* Carico l'albero degli utenti dal file passato come argomento e chiudo il file.
		 Non c'è bisogno di fare lock sui semafori perché ancora non ho creato nessun thread.
	*/
	if ((loadUsers (fp, &AlberoUtenti))==-1)
	{
		perror ("server (main) crashed on loadUser");
		fclose (fp);
		exit (EXIT_FAILURE);
	}

	if ((fclose (fp))!=0)
	{
		perror ("server (main) crashed on fclose 0 arg[1]");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	/* Azzero le strutture per la gestione dei segnali SIGUSR1, SIGINT, SIGTERM e SIGPIPE,
     la inizializzo con le relative funzioni e installo i gestori.
	*/
	bzero (&sa_USR1, sizeof (sa_USR1));
	bzero (&sa_TERM_INT, sizeof (sa_TERM_INT));
  bzero (&sa_PIPE, sizeof (sa_PIPE));
	sa_USR1.sa_handler= &gestore_USR1;
	sa_TERM_INT.sa_handler= &gestore_TERM_INT;
  sa_PIPE.sa_handler= SIG_IGN;

	if ((sigaction (SIGUSR1, &sa_USR1, NULL))==-1)
	{
		perror ("server (main) crashed on sigaction SIGUSR1");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	if ((sigaction (SIGTERM, &sa_TERM_INT, NULL))==-1)
	{
		perror ("server (main) crashed on sigaction SIGTERM");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	if ((sigaction (SIGINT, &sa_TERM_INT, NULL))==-1)
	{
		perror ("server (main) crashed on sigatcion SIGINT");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

  if ((sigaction (SIGPIPE, &sa_PIPE, NULL))==-1)
  {
    perror ("server (main) crashed on sigaction SIGPIPE");
    freeTree (AlberoUtenti);
    exit (EXIT_FAILURE);
  }

	/* Creo la socket sulla quale si connetteranno i client.
	*/
	if ((socket= createServerChannel (BRSSOCKET))==-1)
	{
		perror ("server (main) crashed on createServerChannel");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	/* Tolgo dalla maschera tutti i segnali.
	*/
	if ((sigemptyset (&mask))==-1)
	{
		perror ("server (main) crashed on sigemptyset");
		closeServerChannel (BRSSOCKET, socket);
		freeTree(AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	/* Creo un thread che gestisce le nuove connessioni e gli assegno la funzione thread_disp,
		 passandogli come argomento il file descriptor della socket.
	*/
	if ((err=pthread_create (&TID, NULL, &thread_disp, (void *) socket))!=0)
	{
		errno=err;
		perror ("server (main) crashed on pthread_create thread_disp");
		freeTree (AlberoUtenti);
		closeServerChannel (BRSSOCKET, socket);
		exit (EXIT_FAILURE);
	}

	/* Attendo la terminazione del thread_disp.
	*/
	if ((err=pthread_join (TID, (void **) &retval))!=0)
	{
		errno=err;
		perror ("server (main) crashed on pthread_join thread_disp");
		freeTree (AlberoUtenti);
		closeServerChannel (BRSSOCKET, socket);
		exit (EXIT_FAILURE);
	}

	/* Controllo lo stato di terminazione del thread_disp e chiudo la socket.
		 Se il thread_disp è terminato con errore, lo segnalo ma non termino il server.
	*/
	if (retval!=0)
	{
		errno=*retval;
		perror ("server (main) thread_disp exited with errorr");
	}

	if ((closeServerChannel (BRSSOCKET, socket))==-1)
		perror ("server (main) fails on closeServerChannel");

	/* Il thread_disp è terminato e non accetto più connessioni.
		 Disabilito la ricezione dei segnali e attendo la terminazione di tutti i thread worker.
		 Se la sigfillset termina con errore continuo lo stesso il programma.
		 Prima eseguo una trylock per verificare che il thread in considerazione non sia già terminato, e quindi
		 non abbia fatto già una pthread_detach su sé stesso, e, se questa è andata a buon fine, attendo
		 la terminazione del thread, altrimenti non faccio niente e chiudo il canale di connessione.
	*/
	if ((sigfillset (&mask))!=0)
		perror ("server (main) fails on sigfillset 1");

	app=List;
	while (app!=NULL)
	{
		if ((etry=pthread_mutex_trylock (&app->detached))==0 && (err=pthread_join (app->PID, (void **) &retval))!=0)
		{
			errno=err;
			perror ("server (main) fails on pthread_join thread_worker");
			pthread_cancel (app->PID);
		}

		else if (etry!=EBUSY && etry!=0)
		{
			errno=etry;
			perror ("server (main) fails on pthread_mutex_trylock thread_worker");
			pthread_cancel (app->PID);
		}

		if (retval!=0)
		{
			errno=*retval;
			perror ("server (main) thread_worker exited with error");
		}

		app=app->next;
		if (closeConnection (List->FD)==-1)
			if (errno!=EBADF)
				perror ("server (main) fails on closeConnection");
		free (List);
		List=app;
	}

	/* Stampo l'albero degli utenti su file e termino il server.
		 Non è necessario sospendersi sul mutex mux_tree  perché non ci sono più thread nel processo.
	*/
	if ((fp=fopen (arg[1], "w+"))==NULL)
	{
		perror ("server (main) crashed on fopen 1 arg[1]");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	if ((storeUsers (fp, AlberoUtenti))==-1)
	{
		perror ("server (main) crashed on storeUsers");
		freeTree (AlberoUtenti);
		fclose (fp);
		exit (EXIT_FAILURE);
	}

	if ((fclose (fp))!=0)
	{
		perror ("server (main) crashed on fclose 1 arg[1]");
		freeTree (AlberoUtenti);
		exit (EXIT_FAILURE);
	}

	freeTree (AlberoUtenti);

	return EXIT_SUCCESS;
}





/**
	* Prima ignoro il segnale SIGPIPE e installo il gestore per il segnale SIGUSR2 e, successivamente,
	* mi metto in attesa di nuove connessioni da parte dei client, per ognuna delle quali
	* creo un elemento della lista ::List e un ::thread_worker che gestisce la connessione.
	* Quando la variabile ::accept_connection e uguale a 0, significa che non posso più
	* ricevere connessioni e termino il thread.

		\param arg File descriptor della socket.

		\retval EXIT_SUCCESS Se non ci sono errori.
		\retval errno Il valore di errno se si è verificato un'errore.
*/
void *thread_disp (void *arg)
{
	/* Variabile di appoggio per l'inserimento di un nuovo elemento nella lista. */
	element_t *app=NULL;
	/* Socket sulla quale si connetteranno i client. */
	int socket=0;
	/* Variabile per salvare il valore di errno nei vari errori. */
	int err;
	/* Variabile per indicare se ci sono errori nella acceptConnection. */
	bool_t errAccept=FALSE;
	/* Variabile per indicare se è stata creata la connessione o meno. */
	bool_t connectionCreated= FALSE;
	/* Struttura per la gestione del segnale SIGUSR2. */
	struct sigaction sa_USR2;
  /* Struttura pe la gestione del segnale SIGPIPE. */
  struct sigaction sa_PIPE;
	/* Variabile per indicare quante volte va eseguita la pthread_create in caso di errore. Viene inizializzata al valore della macro N_TRY_THREAD. */
	int times= N_TRY_THREAD;
	/* Struttura per l'invio del messaggio al client in caso di errore. */
	message_t msg;

	/* Ignoro il segnale SIGPIPE, installo il gestore per il segnale SIGUSR2 e setto il valore della variabile "socket".
	*/
  bzero (&sa_PIPE, sizeof (sa_PIPE));
  sa_PIPE.sa_handler= SIG_IGN;

  if ((sigaction (SIGPIPE, &sa_PIPE, NULL))==-1)
  {
    perror ("server (thread_disp) exit with error on sigactio SIGPIPE");
    return (void *) errno;
  }

	bzero (&sa_USR2, sizeof (sa_USR2));
	sa_USR2.sa_handler= &gestore_USR2;

	if ((sigaction (SIGUSR2, &sa_USR2, NULL))==-1)
	{
		perror ("server (thread_disp) exit with error on sigaction SIGUSR2");
		return (void *) errno;
	}

	socket= (int) arg;

	/* Stampo la stringa "*** Server Pronto ***" per informare l'utente che il server ha caricato
		 eseguito correttamente tutte le operazioni fino ad adesso ed è pronto ad accettare conessioni.
	*/
	printf ("\n*** Server Pronto ***\n");

	/* Eseguo un ciclo con il quale accetto le connessioni da parte degli utenti.
		 Prima creo un nuovo elemento della lista dei thread_worker e dopo faccio una acceptConnection
		 e assegno il file descriptor della nuova connessione alla variabile FD all'interno della
		 struttura appena creata. Successivamente creo il thread che si occuperà della connessione con
		 l'utente appena connesso.
	*/
	while (accept_connection)
	{
		errAccept= FALSE;
		connectionCreated= FALSE;
		app=List;
		if ((List= (element_t *) malloc (sizeof(element_t)))==NULL)
		{
			/* Se la allocazione va male, chiudo tutte le connessioni e termino il server.
			*/
			perror ("server (thread_disp) crashed on malloc 0");
			while (app!=NULL)
			{
				closeConnection (app->FD);
				app=app->next;
			}
			exit (EXIT_FAILURE);
		}

		List->next=app;
		pthread_mutex_init (&(List->detached), NULL);
		/* Se la connessione con il client va male, mi rircordo dell'errore avuto nella variabile errAccept,
			 dealloco la memoria relativa all'ultimo elemento della lista inserito e continuo con il programma
			 ma non esco se no avrei da terminare tutti gli altri thread.
		*/
		if ((List->FD= acceptConnection (socket))!=-1)
		{
			connectionCreated=TRUE;
		}

		else
		{
			perror ("server (thread_disp) fails on acceptConnection");
			errAccept= TRUE;
			connectionCreated= FALSE;
			free (List);
			List=app;
		}

		if (!errAccept && connectionCreated)
		{
			/* Eseguo un while dove per "times" volte (variabile inizializzata al valore di N_TRY_THREAD) provo a creare il
				 thread_worker per quel client. Se viene restituito un errore EAGAIN, ci sono troppi thread per questo processo,
				 provo ad eseguire la pthread_create dopo SEC_TRY_THREAD secondi. Se invece da un'altro errore, mando un messaggio
				 al client con la spiegazione dell'errore e mi metto in attesa di nuove richieste di connessione.
			*/
			times= N_TRY_THREAD;
			while (times && (err=pthread_create (&(List->PID), NULL, &thread_worker, (void *) List))!=0)
			{
				if (err==EAGAIN)
				{
					pthread_cancel ((List->PID));
					printf ("Too many threads. Try again\n");
					sleep (SEC_TRY_TREAD);
					times--;
				}
				else
					times=0;
			}
		}
		
		if (!times)
		{
			errno=err;
			perror ("server (thread_disp) fails on pthread_create");
			
			msg.type= MSG_ERR;
			if ((msg.buffer= malloc ((strlen (sys_errlist [err]))+1))==NULL)
			{
				/* Se la allocazione va male, chiudo tutte le connessioni e termino il server.
				*/
				perror ("server (thread_disp) crashed on malloc 1");
				while (app!=NULL)
				{
					closeConnection (app->FD);
					app=app->next;
				}
				exit (EXIT_FAILURE);
			}

			strncpy (msg.buffer, sys_errlist [err], strlen (sys_errlist [err]));
			msg.length= strlen (sys_errlist [err]);

			if ((sendMessage (List->FD, &msg))!=((msg.length)+4+1))
				perror ("server (thread_disp) fails on sendMessage 0");

			free (msg.buffer);

			if ((closeConnection (List->FD))==-1)
				perror ("server (thread_disp) fails on closeConnection");

			free (List);
			List=app;
		}
	}
	return (void *) EXIT_SUCCESS;
}





/** Funzione per la gestione della connessione con i client.
		Per pima cosa maschero tutti i segnali, ricevo il messaggio da parte dell'utente
		e guardo se vuole registrarsi o cancellarsi dall'albero degli utenti oppure fare una partita.
		In base a quello che vuole fare l'utente eseguo le operazioni necessarie.

		\param arg Puntatore alla struttura element_t relativa al client a cui è associato questo thread.

		\retval EXIT_SUCCESS Se tutto è andato a buon fine.
		\retval errno Il valore di errno se si è verificato un'errore.
*/
void *thread_worker (void *arg)
{
	/* Indici di controllo. */
	int i=0, j=0;
	/* Variabile per controllare il valore di ritorno dalle funzioni. */
	int retval=0;
	/* Variabile per salavare il valore di errno dopo un errore in una funzione. */
	int err=0;
	/* Struttura per l'invio e la ricezione dei messaggi.	*/
	message_t msg;
	/* Variabile per memorizzare l'utente connesso al sevizio. */
	user_t *U=NULL;
	/* Stringa per contenere la lista degli utenti disponibili ad una partita. */
	char *UserList=NULL;
	/* Stringa per memorizzare il nome del client relativo	a questo thread	(ovvero colui che lancia la sfida).	*/
	char mainUser[LUSER+1];
	/* Stringa per memorizzare l'utente che ha ricevuto la sfida. */
	char otherUser[LUSER+1];
	/* File descriptor del mainUser. */
	int FD_main;
	/* File descriptor dell'otherUser. */
	int FD_other;
	/* Struttura per la gestione dei messaggi relativi alla partita con il mainUser. */
	message_t msg_main;
	/* Struttura per la gestione dei messaggi relativi alla partita con l'otherUser. */
	message_t msg_other;
	/* Maschera per bloccare i segnali. */
	sigset_t mask;
	/* Puntatore al mazzo della partita. */
	mazzo_t *m=NULL;
	/* Identificatore del numero della partita. */
	int partita=0;
	/* Variabile per memorizzare il numero delle cifre della variaile partita. */
	int cifre_partita=0;
	/* Carattere per identificare il seme della briscola. */
	char briscola;
	/* Variabile per indicare se è il turno dela mainUser o no. */
	bool_t mio_turno=TRUE;
	/* Variabili per memorizzare le carte dei giocatori. */
	carta_t *TMPcard_main=NULL, *TMPcard_other=NULL;
	/* Stringhe per memorizzare le carte dei giocatori (nel formato stringa). */
	char TMPstrcard_main[3], TMPstrcard_other[3];
	/* Interi per memorizzare i punti dei giocatori. */
	int point_main=0, point_other=0;
	/* Puntatore al file di log della partita. */
	FILE *fp=NULL;
	/* Stringa che rappresenta il nome del file. */
	char *namefile=NULL;
	/* Array di carte per memorizzare le carte prese dal mainUser. */
	carta_t *carte_prese_main[40];
	/* Variabile che indica la dimensione dell'array carte_prese_main. */
	int indice_main=0;
	/* Variabili per indicare la carta appena giocata. */
	carta_t *cardPlayed_main=NULL, *cardPlayed_other=NULL;
	/* Variabile per identificare l'argomento della funzione. */
	element_t *tmp= (element_t *) arg;
  /* Variabile per salvare lo stato di un utente. */
  status_t stat;




	/* Assegno al file descriptor dell'utente appena connesso il valore del file descrpitor passato per argomento.
	*/
	FD_main= tmp->FD;

	/* Maschero i segnali SIGPIPE, SIGUSR1 e SIGUSR2.
	*/
	if ((sigaddset (&mask, SIGPIPE))!=0)
	{
		perror ("server (thread_worker) exit with error on sigaddset SIGPIPE");
		err=errno;
		closeConnection (FD_main);
		if ((pthread_mutex_trylock (&tmp->detached))==0)
			pthread_detach (tmp->PID);
		return (void *) err;
	}

	if ((sigaddset (&mask, SIGUSR1))!=0)
	{
		perror ("server (thread_worker) exit with error on sigaddset SIGUSR1");
		err=errno;
		closeConnection (FD_main);
		if ((pthread_mutex_trylock (&tmp->detached))==0)
			pthread_detach (tmp->PID);
		return (void *) err;
	}
	
	if ((sigaddset (&mask, SIGUSR2))!=0)
	{
		perror ("server (thread_worker) exit with error on sigaddset SIGUSR2");
		err=errno;
		closeConnection (FD_main);
		if ((pthread_mutex_trylock (&tmp->detached))==0)
			pthread_detach (tmp->PID);
		return (void *) err;
	}

	/* Ricevo il messaggio dal client appena connesso che mi dice cosa fare ed
		 estraggo dalla stringa del messaggio il nome utente e la password
		 convertendoli in struttura utente. Succerssivamente considero i valori del
		 campo type del messaggio ricevuto e eseguo l'operazione richiesta.
	*/
	if ((receiveMessage (FD_main, &msg))==-1)
	{
		perror ("server (thread_worker) exit with error on receiveMessage 0");
		err=errno;
		closeConnection (FD_main);
		if ((pthread_mutex_trylock (&tmp->detached))==0)
			pthread_detach (tmp->PID);
		return (void *) err;
	}

	if ((U=stringToUser (msg.buffer, msg.length))==NULL)
	{
		perror ("server (thread_worker) exit with error on stringToUser");
		err=errno;
		free (msg.buffer);
		closeConnection (FD_main);
		if ((pthread_mutex_trylock (&tmp->detached))==0)
			pthread_detach (tmp->PID);
		return (void *) err;
	}

	free (msg.buffer);
	msg.buffer=NULL;
	msg.length=0;




	/* Il client ha chiesto di registrarsi nell'albero degli utenti.
	*/
	if (msg.type==MSG_REG)
	{
		/* Ottengo la mutua esclusione sull'albero degli utenti.
		*/
		pthread_mutex_lock (&mux_tree);

		while (workOnTree)
			pthread_cond_wait (&cond_tree, &mux_tree);

		workOnTree= TRUE;

		/*  Aggiungo l'utente nell'AlberoUtenti e considero i valori che mi restutuisce la funzione addUser.
		*/
		if ((retval=addUser (&AlberoUtenti, U))==0)
		{
			/* L'operazione è stata eseguita con successo.
			*/
			msg.type= MSG_OK;
			msg.length=0;
			msg.buffer=NULL;
		}

		else if (retval==1)
		{
			/* Esiste già un utente con quel nome. Copio nel capo buffer del messaggio la stringa
				 identificata dalla macro ALREADYEXIST.
			*/
      msg.type= MSG_NO;
			if ((msg.buffer= malloc ((strlen (ALREADYEXIST)) +1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc 0");
				exit (EXIT_FAILURE);
			}
			strcpy (msg.buffer, ALREADYEXIST);
			msg.length= strlen (msg.buffer);
		}

		else if (retval==-1)
		{
			/* Si è verificato un errore. Copio nel campo buffer del messaggio la stringa
				 corrispondente al valore di errno.
			*/
			msg.type= MSG_NO;
			if ((msg.buffer= malloc ((strlen (sys_errlist[errno]))+1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc1");
				exit (EXIT_FAILURE);
			}
			strncpy (msg.buffer, sys_errlist[errno], strlen (sys_errlist[errno]));
			msg.length= strlen (msg.buffer);
			msg.length=0;
		}

		/* Rilascio la mutua esclusione.
		*/
		workOnTree= FALSE;

		pthread_cond_signal (&cond_tree);
		pthread_mutex_unlock (&mux_tree);

		/* Invio il messaggio all'utente con l'esito dell'operazione. Se l'operazione va male
				libero la memoria e termino il thread.
		*/
		if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
		{
			perror ("server (thread_worker) exit with error on sendMessage 0");
			err=errno;
			closeConnection (FD_main);
			free (msg.buffer);
			if ((pthread_mutex_trylock (&tmp->detached))==0)
				pthread_detach (tmp->PID);
			return (void *) err;
		}

		/* Chiudo la connessione con il client.
		*/
		if ((closeConnection (FD_main))==-1)
			perror ("server (thread_worker) fails on closeConnection 0");

    /* Libero la memoria e termino il thread.
    */
    free (msg.buffer);
    closeConnection (FD_main);

    if ((pthread_mutex_trylock (&tmp->detached))==0)
		pthread_detach (tmp->PID);

    return (void *) EXIT_SUCCESS;
	}




	/* Il client ha chiesto di cancellarsi dall'albero degli utenti.
	*/
	else if (msg.type==MSG_CANC)
	{
		/* Ottengo la mutua esclusione sull'albero degli utenti.
		*/
		pthread_mutex_lock (&mux_tree);

		while (workOnTree)
			pthread_cond_wait (&cond_tree, &mux_tree);

		workOnTree= TRUE;

		/* Controllo che l'utente non sia coinvolto in una partia o in stato WAIT.
		*/
		if (((stat=getUserStatus (AlberoUtenti, U->name))==PLAYING) || (stat==WAITING))
		{
			msg.type= MSG_NO;
			if ((msg.buffer= malloc ((strlen (ALREADYCONNECT)) +1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc 2");
				exit (EXIT_FAILURE);
			}
			strncpy (msg.buffer, ALREADYCONNECT, strlen (ALREADYCONNECT));
			msg.length= strlen (msg.buffer);
		}

		/* L'utente non è coinvolto in una partita e non è in stato WAIT.
			 Rimuovo l'utente dall'AlberoUtenti e considero i valori che mi restutuisce la funzione removeUser.
		*/
		else if ((retval= removeUser (&AlberoUtenti, U))==0)
		{
			/* Operazione avvenuta correttamente.
			*/
			msg.type= MSG_OK;
			msg.length=0;
			msg.buffer=NULL;
		}

		else if (retval == -1)
		{
			/* Si è verificato un errore. Copio nel campo buffer del messaggio la stringa
				 corrispondente al valore di errno.
			*/
			msg.type= MSG_NO;
			if ((msg.buffer= malloc ((strlen (sys_errlist[errno]))+1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc 3");
				exit (EXIT_FAILURE);
			}
			strncpy (msg.buffer, sys_errlist[errno], strlen (sys_errlist[errno]));
			msg.length= strlen (msg.buffer);
		}

		else if (retval== NOUSR)
		{
			/* Utente non presente.
			*/
			msg.type= MSG_NO;
			if ((msg.buffer= malloc ((strlen (NOUSER))+1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc 4");
				exit (EXIT_FAILURE);
			}
			strncpy (msg.buffer, NOUSER, strlen (NOUSER));
			msg.length= strlen (msg.buffer);
		}

		else if (retval == WRPWD )
		{
			/* Password sbagliata.
			*/
			msg.type= MSG_ERR;
			if ((msg.buffer= malloc ((strlen (WRONGPWD))+1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc 5");
				exit (EXIT_FAILURE);
			}
			strncpy (msg.buffer, WRONGPWD, strlen (WRONGPWD));
			msg.length= strlen (msg.buffer);
		}

		/* Rilascio la mutua esclusione.
		*/
		workOnTree= FALSE;

		pthread_cond_signal (&cond_tree);
		pthread_mutex_unlock (&mux_tree);

		/* Invio il messaggio all'utente con l'esito dell'operazione. Se la sendMessage va male,
			 libero la memoria e termino il thread.
		*/
		if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
		{
			perror ("server (thread_worker) exit with error on sendMessage 1");
			err=errno;
			closeConnection (FD_main);
			free (msg.buffer);
			if ((pthread_mutex_trylock (&tmp->detached))==0)
				pthread_detach (tmp->PID);
			return (void *) err;
		}

		/* Chiudo la connessione con il client.
		*/
		if ((closeConnection (FD_main))==-1)
			perror ("server (thread_worker) fails on closeConnection 1");

    /* Libero la memoria e termino il thread.
    */
    free (msg.buffer);
    closeConnection (FD_main);

    if ((pthread_mutex_trylock (&tmp->detached))==0)
		pthread_detach (tmp->PID);

    return (void *) EXIT_SUCCESS;
	}




	/* L'utente vuole connettersi al servizio e fare una partita.
	*/
	else if (msg.type==MSG_CONNECT)
	{
		/* Ottengo la mutua esclusione sull'albero degli utenti.
		*/
		pthread_mutex_lock (&mux_tree);
		while (workOnTree)
			pthread_cond_wait (&cond_tree, &mux_tree);

		workOnTree= TRUE;


		/* Controllo che l'utente sia presente nell'albero e che la password sia giusta.
		*/
		if ((checkPwd (AlberoUtenti, U))==FALSE)
		{
			/* L'utente non è presente nell'AlberoUtenti oppure la password è sbagliata.
				 Guardo quale dei due errori va segnalato al client e gli invio il messaggio.
			*/
			if ((isUser (AlberoUtenti, U->name))==FALSE)
			{
				/* L'utente non è presente nell'AlberoUtenti.
				*/
				msg.type= MSG_NO;
				if ((msg.buffer= malloc ((strlen (NOUSER))+1))==NULL)
				{
					perror ("server (thread_worker) crashed on malloc 6");
					exit (EXIT_FAILURE);
				}
				strncpy (msg.buffer, NOUSER, strlen (NOUSER));
				msg.length= strlen (msg.buffer);
			}

			else
			{
				/* L'utente è presente nell'AlberoUTenti quindi necessariamente la password è sbagliata.
				*/
				msg.type= MSG_ERR;
				if ((msg.buffer= malloc ((strlen (WRONGPWD))+1))==NULL)
				{
					perror ("server (thread_worker) crashed on malloc 7");
					exit (EXIT_FAILURE);
				}
				strncpy (msg.buffer, WRONGPWD, strlen (WRONGPWD));
				msg.length= strlen (msg.buffer);
			}

			/* Rilascio la mutua esclusione e invio il messaggio.
			*/
			workOnTree= FALSE;

			pthread_cond_signal (&cond_tree);
			pthread_mutex_unlock (&mux_tree);

			if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
			{
				perror ("server (thread_worker) exit with error on sendMessage 2");
				err=errno;
				closeConnection (FD_main);
				free (msg.buffer);
				if ((pthread_mutex_trylock (&tmp->detached))==0)
					pthread_detach (tmp->PID);
				return (void *) err;
			}

      /* Libero la memoria e termino il thread.
      */
			free (msg.buffer);
			closeConnection (FD_main);

			if ((pthread_mutex_trylock (&tmp->detached))==0)
				pthread_detach (tmp->PID);

			return (void *) 0;
			
		}

		/* L'utente è presente nell'AlberoUtenti. Gli invio la lista degli avversari disponibili.
		*/
		else
		{
			/* Controllo che l'utente non sia già connesso (status== WAITING/PLAYED) nell'albero.
				 Se è vero, segnalo l'errore al client e termino il thread.
			*/
			if ((getUserStatus (AlberoUtenti, U->name))!=DISCONNECTED)
			{
				msg.type= MSG_NO;
				if ((msg.buffer= malloc ((strlen (ALREADYCONNECT))+1))==NULL)
				{
					perror ("server (thread_worker) crashed on malloc 8");
					exit (EXIT_FAILURE);
				}
				strncpy (msg.buffer, ALREADYCONNECT, strlen (ALREADYCONNECT));
				msg.length= strlen (msg.buffer);

				/* Rilascio la mutua esclusione e invio il messaggio.
				*/
				workOnTree= FALSE;

				pthread_cond_signal (&cond_tree);
				pthread_mutex_unlock (&mux_tree);

				if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
				{
					perror ("server (thread_worker) exit with error on sendMessage 3");
					err=errno;
					free (msg.buffer);
					closeConnection (FD_main);
					if ((pthread_mutex_trylock (&tmp->detached))==0)
						pthread_detach (tmp->PID);
					return (void *) err;
				}

				free (msg.buffer);
				if ((closeConnection (FD_main))==-1)
					perror ("server (thread_worker) fails on closeConnection 2");
				if ((pthread_mutex_trylock (&tmp->detached))==0)
					pthread_detach (tmp->PID);
				return (void *) 0;
			}

			else
			{
				/* L'utente è disconnesso. Controllo se ci sono avversari disponibili per una
					 partita. Se è vero li inserisco nel campo buffer del messaggio.
				*/
				strcpy (mainUser, U->name);
				
				if ((UserList=getUserList (AlberoUtenti, WAITING))==NULL)
				{
					/* Non ci sono avversari disponibili.
					*/
					msg.type= MSG_WAIT;
					msg.buffer= NULL;
					msg.length= 0;
				}

				else
				{
					/* Ci sono avversari in attesa di essere sfidati.
					*/
					msg.type= MSG_OK;
					if ((msg.buffer= malloc (strlen (UserList)+1))==NULL)
					{
						perror ("server (thread_worker) crashed on malloc 9");
						exit (EXIT_FAILURE);
					}
					strncpy (msg.buffer, UserList, strlen (UserList));
					msg.length= strlen (msg.buffer);
				}
			}
		}

		/* Rilascio la mutua esclusione e invio il messaggio al client con la lista degli avversari disponibili.
		*/
		workOnTree= FALSE;

		pthread_cond_signal (&cond_tree);
		pthread_mutex_unlock (&mux_tree);

		if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
		{
			perror ("server (thread_worker) exit with error on sendMessage 4");
			err=errno;
			free (msg.buffer);
			closeConnection (FD_main);
			if ((pthread_mutex_trylock (&tmp->detached))==0)
				pthread_detach (tmp->PID);
			return (void *) err;
		}

		free (msg.buffer);
		msg.length= 0;
		msg.buffer= NULL;


		/* Non ci sono utenti disponibili per fare una partita.
			 Il thread segnala lo stato e il canale dell'utente sull'albero e termina.
		*/
		if (UserList==NULL)
		{
			/* Ottengo la mutua esclusione sull'albero degli utenti.
			*/
			pthread_mutex_lock (&mux_tree);
			while (workOnTree)
				pthread_cond_wait (&cond_tree, &mux_tree);

			workOnTree= TRUE;

			/* Se una delle due funzioni non termina con successo fallisce il server poiché
				 potrebbe non poter accedere all'AlberoUtenti.
			*/
			if ((setUserChannel (AlberoUtenti, mainUser, FD_main))==FALSE)
			{
				perror ("server (thread_worker) crashed on setUserChannel 0");
				exit (EXIT_FAILURE);
			}
			if ((setUserStatus (AlberoUtenti, mainUser, WAITING))==FALSE)
			{
				perror ("server (thread_worker) crahed on setUserStatus 0");
				exit (EXIT_FAILURE);
			}

			/* Rilascio la mutua esclusione e termino il thread.
			*/
			workOnTree= FALSE;
			
			pthread_cond_signal (&cond_tree);
			pthread_mutex_unlock (&mux_tree);
			
			if ((pthread_mutex_trylock (&tmp->detached))==0)
				pthread_detach (tmp->PID);
			return (void *) 0;
		}

		/* Ci sono utenti disponibili per fare una partita.
			 Mi metto in attesa di un messaggio da parte del client che mi dice cosa vuole fare.
		*/
		else
		{
			free (UserList);
			UserList= NULL;

			if ((receiveMessage (FD_main, &msg))==-1)
			{
				perror ("server (thread_worker) exit with error on receiveMessage 1");
				err=errno;
				free (msg.buffer);
				closeConnection (FD_main);
				if ((pthread_mutex_trylock (&tmp->detached))==0)
					pthread_detach (tmp->PID);
				return (void *) errno;
			}

			/* L'utente vuole aspettare uno sfidante.
				 Setto lo stato del giocatore a WAITING, il suo canale con l'indirizzo della socket e termino il thread.
			*/
			if (msg.type== MSG_WAIT)
			{
				/* Ottengo la mutua esclusione sull'albero degli utenti. */
				pthread_mutex_lock (&mux_tree);
				while (workOnTree)
					pthread_cond_wait (&cond_tree, &mux_tree);

				workOnTree= TRUE;

				/* Se una delle due funzioni non termina con successo fallisce il server poiché
					 potrebbe non poter accedere all'AlberoUtenti.
				*/
				if ((setUserChannel (AlberoUtenti, mainUser, FD_main))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserChannel 1");
					exit (EXIT_FAILURE);
				}
				if ((setUserStatus (AlberoUtenti, mainUser, WAITING))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserStatus 1");
					exit (EXIT_FAILURE);
				}

				/* Rilascio la mutua esclusione e termino il thread.
				*/
				workOnTree= FALSE;

				pthread_cond_signal (&cond_tree);
				pthread_mutex_unlock (&mux_tree);

				free (msg.buffer);
				msg.buffer=NULL;
				if ((pthread_mutex_trylock (&tmp->detached))==0)
					pthread_detach (tmp->PID);
				return (void *) 0;
			}


			/* L'utente ha aperto una sfida con un altro giocatore.
				 Setto lo stato del giocatore che ha lanciato la sfida e il suo canale,
				 setto lo stato del gicatore che era in attesa e ottengo il suo canale.
			*/
			else if (msg.type== MSG_OK)
			{
				/* Ottengo la mutua esclusione sull'albero degli utenti.
				*/
				pthread_mutex_lock (&mux_tree);
				while (workOnTree)
					pthread_cond_wait (&cond_tree, &mux_tree);

				workOnTree= TRUE;

				/* Controllo che l'utente sfidato sia ancora nell'albero, connesso e in stato WAIT.
					 Se non è così invio un messaggio con l'errore al client e termino il thread.
				*/
				if ((getUserStatus (AlberoUtenti, msg.buffer))!=WAITING)
				{
					msg.type= MSG_NO;
					free (msg.buffer);
					msg.buffer= NULL;
					if ((msg.buffer= malloc ((strlen (USERUNAVAILABLE))+1))==NULL)
					{
						perror ("server (thread_worker) crashed on malloc 10");
						exit (EXIT_FAILURE);
					}
					strncpy (msg.buffer, USERUNAVAILABLE, strlen (USERUNAVAILABLE));
					msg.length= strlen (msg.buffer);

					/* Rilascio la mutua esclusione sull'AlberoUtenti, invio il messaggio e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
					{
						perror ("server (thread_worker) exit with error on sendMessage 5");
						err=errno;
						free (msg.buffer);
						closeConnection (FD_main);
						if ((pthread_mutex_trylock (&tmp->detached))==0)
							pthread_detach (tmp->PID);
						return (void *) err;
					}

					free (msg.buffer);
					if ((pthread_mutex_trylock (&tmp->detached))==0)
						pthread_detach (tmp->PID);
					return (void *) 0;
				}


				/* Controllo che l'utente sfidante sia ancora disconesso.
					 Se non è così invio un messaggio al client e termino il thread.
				*/
				if ((getUserStatus (AlberoUtenti, mainUser))!=DISCONNECTED)
				{
					msg.type= MSG_NO;
					free (msg.buffer);
					if ((msg.buffer= malloc ((strlen (ALREADYCONNECT))+1))==NULL)
					{
						perror ("server (thread_worker) crashed on malloc 11");
						exit (EXIT_FAILURE);
					}
					strncpy (msg.buffer, ALREADYCONNECT, strlen (ALREADYCONNECT));
					msg.length= strlen (msg.buffer);

					/* Rilascio la mutua esclusione, invio il messaggio e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					if ((sendMessage (FD_main, &msg))!=((msg.length)+4+1))
					{
						perror ("server (thread_worker) exit with error on sendMessage 6");
						err=errno;
						free (msg.buffer);
						closeConnection (FD_main);
						if ((pthread_mutex_trylock (&tmp->detached))==0)
							pthread_detach (tmp->PID);
						return (void *) err;
					}

					free (msg.buffer);
					if ((pthread_mutex_trylock (&tmp->detached))==0)
						pthread_detach (tmp->PID);
					return (void *) 0;
				}

				strcpy (otherUser, msg.buffer);
				
				/* Setto lo stato degli utenti come PLAYING e ottengo il valore del canale
					 su cui è connesso l'utente sfidato.
					 Se una delle tre funzioni non termina con successo fallisce il server poiché
					 potrebbe non poter accedere all'AlberoUtenti.
				*/
				if ((setUserStatus (AlberoUtenti, otherUser, PLAYING))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserStatus 2");
					exit (EXIT_FAILURE);
				}

				if ((setUserStatus (AlberoUtenti, mainUser, PLAYING))==FALSE)
				{
					perror ("server (thread_worker) crrashed on setUserStatus 3");
					exit (EXIT_FAILURE);
				}

				if ((FD_other= getUserChannel (AlberoUtenti, otherUser))==NOTREG)
				{
					perror ("server (thread_worker) crashed on getUserChannel");
					exit (EXIT_FAILURE);
				}

				free (msg.buffer);

				/* Rilascio la mutua esclusione.
				*/
				workOnTree= FALSE;

				pthread_cond_signal (&cond_tree);
				pthread_mutex_unlock (&mux_tree);



				/* Comincia la partita.
					 Mischio il mazzo, pesco le carte per i due client e invio ai client i messaggi con le informazioni necessarie.
					 Il messaggio di inizio partita è del formato "avversario:briscola:carta1:carta2:carta3"
				*/

				m= newMazzo_r (test);

				if (m->briscola==CUORI)
					briscola='C';
				else if (m->briscola==QUADRI)
					briscola='Q';
				else if (m->briscola==FIORI)
					briscola='F';
				else
					briscola='P';

				if ((msg_main.buffer= malloc ((strlen (otherUser)) +1+6+4+1))==NULL)
				{
					perror ("server (thread_worker) crashed on malloc 12");
					exit (EXIT_FAILURE);
				}
				msg_main.type= MSG_STARTGAME;
				if ((sprintf (msg_main.buffer, "%s:%c", otherUser, briscola))<=0)
				{
					perror ("server (thread_worker) exit with error on sprintf 0");
					err=errno;

					/* Ottengo la mutua esclusione sull'albero degli utenti */
					pthread_mutex_lock (&mux_tree);
					while (workOnTree)
						pthread_cond_wait (&cond_tree, &mux_tree);

					workOnTree= TRUE;

					/* Se una delle funzioni non termina con successo fallisce il server poiché
						 potrebbe non poter accedere all'AlberoUtenti.
					*/
					if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) || ((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserStatus 4");
						exit (EXIT_FAILURE);
					}

					if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserChannel 2");
						exit (EXIT_FAILURE);
					}

					/* Rilascio la mutua esclusione, libero memoria e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					free (msg_main.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}

				if ((msg_other.buffer= malloc ((strlen (mainUser)) +1+6+4+1))==NULL)
				{
					perror ("server (thread_worker) crashed on malloc 13");
					exit (EXIT_FAILURE);
				}

				msg_other.type= MSG_STARTGAME;

				if ((sprintf (msg_other.buffer, "%s:%c", mainUser, briscola))<=0)
				{
					perror ("server (thread_worker) exit with error on sprintf 1");
					err=errno;

					/* Ottengo la mutua esclusione sull'albero degli utenti */
					pthread_mutex_lock (&mux_tree);
					while (workOnTree)
						pthread_cond_wait (&cond_tree, &mux_tree);

					workOnTree= TRUE;

					/* Se una delle funzioni non termina con successo fallisce il server poiché
						 potrebbe non poter accedere all'AlberoUtenti.
					*/
					if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) || ((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserStatus 4");
						exit (EXIT_FAILURE);
					}

					if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserChannel 2");
						exit (EXIT_FAILURE);
					}

					/* Rilascio la mutua esclusione, libero memoria e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					free(msg_main.buffer);
					free(msg_other.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}


				/* Pesco le carte della prima mano, le trasformo in stringa e le copio sul campo buffer del messaggio.
					 Se la funzione getCard restituisce errore, l'unico motivo è per mancanza di memoria,
					 quindi termino il thread e ripristino la situazione iniziale dei due utenti liberando memoria.
				*/
				for (i=0;i<3;i++)
				{
					if (((TMPcard_main= getCard (m))==NULL) || ((TMPcard_other= getCard (m))==NULL))
					{
						perror ("server (thread_worker) exit with error on getCard 0");
						err=errno;

						/* Ottengo la mutua esclusione sull'albero degli utenti */
						pthread_mutex_lock (&mux_tree);
						while (workOnTree)
							pthread_cond_wait (&cond_tree, &mux_tree);

						workOnTree= TRUE;

						/* Se una delle funzioni non termina con successo fallisce il server poiché
							 potrebbe non poter accedere all'AlberoUtenti.
						*/
						if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) || ((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
						{
							perror ("server (thread_worker) crashed on setUserStatus 4");
							exit (EXIT_FAILURE);
						}

						if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
						{
							perror ("server (thread_worker) crashed on setUserChannel 2");
							exit (EXIT_FAILURE);
						}

						/* Rilascio la mutua esclusione, libero memoria e termino il thread.
						*/
						workOnTree= FALSE;

						pthread_cond_signal (&cond_tree);
						pthread_mutex_unlock (&mux_tree);

						free (msg_main.buffer);
						free (msg_other.buffer);
						freeMazzo (m);

						closeConnection (FD_main);
						closeConnection (FD_other);

						if ((pthread_mutex_trylock (&tmp->detached))==0)
							pthread_detach (tmp->PID);
						return (void *) err;
					}

					/* Converto in stringhe le carte appena pescate e le stampo nul campo buffer dei messaggi.
					*/
					cardToString (TMPstrcard_main, TMPcard_main);
					cardToString (TMPstrcard_other, TMPcard_other);

					sprintf (msg_main.buffer, "%s:%s", msg_main.buffer, TMPstrcard_main);
					sprintf (msg_other.buffer, "%s:%s", msg_other.buffer, TMPstrcard_other);

					free (TMPcard_main);
					free (TMPcard_other);
				}

				msg_main.length= strlen (msg_main.buffer);
				msg_other.length= strlen (msg_other.buffer);

				/* Invio i messaggi ai giocatori. Nel caso non riesco a inviare completamente almeno
					 uno dei due messaggi, ripristino la situazione iniziale prima della connessione
					 dei due client. Ovvero setto gli stati degli utenti e il loro canali di connessione,
					 chiudo quest'ultimi e termino il thread.
				*/
				if (((sendMessage (FD_main, &msg_main))!=((msg_main.length)+4+1)) || ((sendMessage (FD_other, &msg_other))!=((msg_other.length)+4+1)))
				{
					perror ("server (thread_worker) exit with error on sendMessage 7");
					err=errno;

					/* Ottengo la mutua esclusione sull'albero degli utenti */
					pthread_mutex_lock (&mux_tree);
					while (workOnTree)
						pthread_cond_wait (&cond_tree, &mux_tree);

					workOnTree= TRUE;

					/* Se una delle funzioni non termina con successo fallisce il server poiché
						 potrebbe non poter accedere all'AlberoUtenti.
					*/
					if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) || ((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
					{
						perror ("server (thread_worker)crashed on setUserStatus 5");
						exit (EXIT_FAILURE);
					}

					if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserChannel 3");
						exit (EXIT_FAILURE);
					}

					/* Rilascio la mutua esclusione, libero memoria e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					free (msg_main.buffer);
					free (msg_other.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					if ((pthread_mutex_trylock (&tmp->detached))==0)
						pthread_detach (tmp->PID);
					return (void *) err;
				}

				
				free (msg_main.buffer);
				free (msg_other.buffer);

				/* Creo il file di log della partita. Se l'operazione va male ripristino la situazione
					 iniziale prima della connessione dei due client, liberando memoria settando stato e canali
					 dei due utenti.
				*/
				partita= n_partita++;
				cifre_partita= ((int) log10 ((double) partita)) +1;
				if ((namefile= malloc ((cifre_partita)+8+1))==NULL)
				{
					perror ("server (thread_worker) crashed on malloc 14");
					exit (EXIT_FAILURE);
				}

				sprintf (namefile, "BRS-%d.log", partita);
				if ((fp= fopen (namefile, "w+"))==NULL)
				{
					perror ("server (thead_worker) exit with error on fopen");
					err=errno;

					/* Ottengo la mutua esclusione sull'albero degli utenti */
					pthread_mutex_lock (&mux_tree);
					while (workOnTree)
						pthread_cond_wait (&cond_tree, &mux_tree);

					workOnTree= TRUE;

					/* Se una delle funzioni non termina con successo fallisce il server poiché
						 potrebbe non poter accedere all'AlberoUtenti.
					*/
					if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserStatus 6");
						exit (EXIT_FAILURE);
					}

					if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserChanne l4");
						exit (EXIT_FAILURE);
					}

					/* Rilascio la mutua esclusione, libero emoria e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					free (msg_main.buffer);
					free (msg_other.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					if ((pthread_mutex_trylock (&tmp->detached))==0)
						pthread_detach (tmp->PID);
					return (void *) err;
				}

				free (namefile);

				if ((fprintf (fp, "%s:%s\nBRISCOLA:%c\n", mainUser, otherUser, briscola))<0)
				{
					perror ("server (thread_worker) exit with error on fprintf 0");
					err=errno;

					/* Ottengo la mutua esclusione sull'albero degli utenti */
					pthread_mutex_lock (&mux_tree);
					while (workOnTree)
						pthread_cond_wait (&cond_tree, &mux_tree);

					workOnTree= TRUE;

					/* Se una delle funzioni non termina con successo fallisce il server poiché
						 potrebbe non poter accedere all'AlberoUtenti.
					*/
					if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserStatus 7");
						exit (EXIT_FAILURE);
					}

					if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
					{
						perror ("server (thread_worker) crashed on setUserChannel 5");
						exit (EXIT_FAILURE);
					}

					/* Rilascio la mutua esclusione, libero memoria e termino il thread.
					*/
					workOnTree= FALSE;

					pthread_cond_signal (&cond_tree);
					pthread_mutex_unlock (&mux_tree);

					free (msg_main.buffer);
					free (msg_other.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					if ((pthread_mutex_trylock (&tmp->detached))==0)
						pthread_detach (tmp->PID);
					return (void *) err;
				}


				/* Comincia il mainUser poiché è quello che ha lanciato la sfida. */
				msg.buffer= NULL;
				if (((msg_main.buffer= malloc (4+1))==NULL) ||	((msg_other.buffer= malloc (4+1))==NULL))
				{
					perror ("server (thread_worker) crashed on malloc 15");
					exit (EXIT_FAILURE);
				}
				i=N_TURNI;
				j=0;
				mio_turno= TRUE;
				while (i)
				{
					/* È il turno del mainUser. Ricevo il messaggio che mi manda il mainUser con la carta giocata e
						 mando il messaggio con la carta giocata dal mainUser all'otherUser.
					*/
					if (mio_turno)
					{
						if (((receiveMessage (FD_main, &msg))==-1) || ((sendMessage (FD_other, &msg))!=((msg.length)+4+1)))
						{
							perror ("server (thread_worker) exit with error on receiveMessage2-sendMessage 8");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti */
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 8");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 6");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero emoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}

						
						/* Salvo in cardPlayed_main la carta appena giocata.
						*/
						if ((cardPlayed_main= stringToCard (msg.buffer))==NULL)
						{
							perror ("server (thread_worker) crashed on stringToCard 0");
							exit (EXIT_FAILURE);
						}

						/* Stampo sul file di log la carta giocata dal mainUser.
							 Se l'operazione non ha successo ripristino la situazione iniziale
							 dei due utenti, libero memoria e termino la partita.
						*/
						if ((fprintf (fp, "%s:%s#", mainUser, msg.buffer))<0)
						{
							perror ("server (thread_worker) exit with error on fprintf 1");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti */
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 9");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 7");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero emoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg.buffer);
							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}

						free (msg.buffer);
						msg.buffer= NULL;
						msg.length=0;

						/* Ricevo il messaggio con la carta giocata dall'otherUser e mando
							 il messaggio con la carta giocata dall'otherUser al mainUser.
						*/

						if (((receiveMessage (FD_other, &msg))==-1) || ((sendMessage (FD_main, &msg))!=((msg.length)+4+1)))
						{
							perror ("server (thread_worker) exit with error on receiveMessage3-sendMessage 9");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti */
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 10");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 8");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero emoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}


						/* Salvo in cardPlayed_other la carta appena giocata.
						*/
						if ((cardPlayed_other= stringToCard (msg.buffer))==NULL)
						{
							perror ("server (thread_worker) crashed on stringToCard 1");
							exit (EXIT_FAILURE);
						}

						/* Stampo sul file di log la carta giocata dall'otherUser.
							 Se l'operazione non ha successo ripristino la situazione iniziale
							 dei due utenti, libero memoria e termino la partita.
						*/
						if ((fprintf (fp, "%s:%s\n", otherUser, msg.buffer))<0)
						{
							perror ("server (thread_worker) exit with error on fprintf 2");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti.
							*/
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 11");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 9");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero emoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg.buffer);
							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}

						free (msg.buffer);
						msg.buffer= NULL;
						msg.length= 0;
					}

					else
					{
						/* È il turno dell'otherUser. Ricevo il messaggio che mi manda l'otherUser con la carta giocata
							 e mando il messaggio con la carta giocata dall'otherUser al mainUser.
						*/
						if (((receiveMessage (FD_other, &msg))==-1) || ((sendMessage (FD_main, &msg))!=((msg.length)+4+1)))
						{
							perror ("server (thread_worker) exit with error on receiveMessage4-sendMessage 10");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti.
							*/
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 12");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 10");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero emoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}
						
						/* Salvo in cardPlayed_other la carta appena giocata.
						*/
						if ((cardPlayed_other= stringToCard (msg.buffer))==NULL)
						{
							perror ("server (thread_worker) crashed on stringToCard 2");
							exit (EXIT_FAILURE);
						}

						/* Stampo sul file di log la carta giocata dall'otherUser.
							 Se l'operazione non ha successo ripristino la situazione iniziale
						   dei due utenti, libero memoria e termino la partita.
						*/
						if ((fprintf (fp, "%s:%s#", otherUser, msg.buffer))<0)
						{
							perror ("server (thread_worker) exit with error on fprintf 3");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti.
							*/
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 13");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 11");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero memoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg.buffer);
							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}

						free (msg.buffer);
						msg.buffer=NULL;
						msg.length=0;

						/* Ricevo il messaggio con la carta giocata dal mainUser e
							 mando il messaggio con la carta giocata dal mainUser all'otherUser.
						*/
						if (((receiveMessage (FD_main, &msg))==-1) || ((sendMessage (FD_other, &msg))!=((msg.length)+4+1)))
						{
							perror ("server (thread_worker) exit with error on receiveMessage5-sendMessage 11");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti.
							*/
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 14");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 12");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero memoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}

						/* Salvo in cardPlayed_main la carta appena giocata.
						*/
						if ((cardPlayed_main= stringToCard (msg.buffer))==NULL)
						{
							perror ("server (thread_worker) crashed on stringToCard 3");
							exit (EXIT_FAILURE);
						}

						/* Stampo sul file di log la carta giocata dal mainUser.
							 Se l'operazione non ha successo ripristino la situazione iniziale
							 dei due utenti, libero memoria e termino la partita.
						*/
						if ((fprintf (fp, "%s:%s\n", mainUser, msg.buffer))<0)
						{
							perror ("server (thread_worker) exit with error on fprintf 4");
							err=errno;

							/* Ottengo la mutua esclusione sull'albero degli utenti.
							*/
							pthread_mutex_lock (&mux_tree);
							while (workOnTree)
								pthread_cond_wait (&cond_tree, &mux_tree);

							workOnTree= TRUE;

							/* Se una delle funzioni non termina con successo fallisce il server poiché
								 potrebbe non poter accedere all'AlberoUtenti.
							*/
							if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserStatus 15");
								exit (EXIT_FAILURE);
							}

							if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
							{
								perror ("server (thread_worker) crashed on setUserChannel 13");
								exit (EXIT_FAILURE);
							}

							/* Rilascio la mutua esclusione, libero memoria e termino il thread.
							*/
							workOnTree= FALSE;

							pthread_cond_signal (&cond_tree);
							pthread_mutex_unlock (&mux_tree);

							free (msg.buffer);
							free (msg_main.buffer);
							free (msg_other.buffer);
							freeMazzo (m);

							closeConnection (FD_main);
							closeConnection (FD_other);

							if ((pthread_mutex_trylock (&tmp->detached))==0)
								pthread_detach (tmp->PID);
							return (void *) err;
						}

						free (msg.buffer);
						msg.buffer=NULL;
						msg.length=0;
					}


					bzero (msg_main.buffer, sizeof (msg_main.buffer));
					bzero (msg_other.buffer, sizeof (msg_other.buffer));
					
					/* Guardo chi deve cominciare il prossimo turno e preparo i messaggi per dire ai client chi deve iniziare la mano seguente.
					*/
					if ((mio_turno && (compareCard (m->briscola, cardPlayed_main, cardPlayed_other))) || (!mio_turno && !(compareCard (m->briscola, cardPlayed_other, cardPlayed_main))))
					{
						mio_turno=TRUE;
						/* Controllo che non siano le ultime tre mani, altrimenti non pesco carte.
						*/
						if (m->next<40)
						{
							if ((TMPcard_main= getCard (m))==NULL)
							{
								perror ("server (thread_worker) exit with error on getCard 1");
								err=errno;

								/* Ottengo la mutua esclusione sull'albero degli utenti.
								*/
								pthread_mutex_lock (&mux_tree);
								while (workOnTree)
									pthread_cond_wait (&cond_tree, &mux_tree);

								workOnTree= TRUE;

								/* Se una delle funzioni non termina con successo fallisce il server poiché
									 potrebbe non poter accedere all'AlberoUtenti.
								*/
								if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserStatus 16");
									exit (EXIT_FAILURE);
								}

								if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserChannel 14");
									exit (EXIT_FAILURE);
								}

								/* Rilascio la mutua esclusione, libero memoria e termino il thread.
								*/
								workOnTree= FALSE;

								pthread_cond_signal (&cond_tree);
								pthread_mutex_unlock (&mux_tree);

								free (msg_main.buffer);
								free (msg_other.buffer);
								freeMazzo (m);

								closeConnection (FD_main);
								closeConnection (FD_other);

								if ((pthread_mutex_trylock (&tmp->detached))==0)
									pthread_detach (tmp->PID);
								return (void *) err;
							}

							cardToString (TMPstrcard_main, TMPcard_main);
							sprintf (msg_main.buffer, "t:%s", TMPstrcard_main);
							msg_main.type= MSG_CARD;
							msg_main.length= strlen (msg_main.buffer);
							free (TMPcard_main);
							TMPcard_main=NULL;

							if ((TMPcard_other= getCard (m))==NULL)
							{
								perror ("server (thread_worker) exit with error on getCard 2");
								err=errno;

								/* Ottengo la mutua esclusione sull'albero degli utenti.
								*/
								pthread_mutex_lock (&mux_tree);
								while (workOnTree)
									pthread_cond_wait (&cond_tree, &mux_tree);

								workOnTree=TRUE;

								/* Se una delle funzioni non termina con successo fallisce il server poiché
										potrebbe non poter accedere all'AlberoUtenti.
								*/
								if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserStatus 17");
									exit (EXIT_FAILURE);
								}

								if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserChannel 15");
									exit (EXIT_FAILURE);
								}

								/* Rilascio la mutua esclusione, libero memoria e termino il thread.
								*/
								workOnTree= FALSE;

								pthread_cond_signal (&cond_tree);
								pthread_mutex_unlock (&mux_tree);

								free (msg_main.buffer);
								free (msg_other.buffer);
								freeMazzo (m);

								closeConnection (FD_main);
								closeConnection (FD_other);

								if ((pthread_mutex_trylock (&tmp->detached))==0)
									pthread_detach (tmp->PID);
								return (void *) err;
							}

							cardToString (TMPstrcard_other, TMPcard_other);
							sprintf (msg_other.buffer, "a:%s", TMPstrcard_other);
							msg_other.type= MSG_CARD;
							msg_other.length= strlen (msg_other.buffer);
							free (TMPcard_other);
							TMPcard_other=NULL;
						}

						else
						{
							sprintf (msg_main.buffer, "t");
							msg_main.type= MSG_CARD;
							msg_main.length= strlen (msg_main.buffer);

							sprintf (msg_other.buffer, "a");
							msg_other.type= MSG_CARD;
							msg_other.length= strlen (msg_other.buffer);
						}

						carte_prese_main[indice_main]= cardPlayed_main;
						indice_main++;
						carte_prese_main[indice_main]= cardPlayed_other;
						indice_main++;
					}

					else
					{
						mio_turno=FALSE;
						/* Controllo che non siano finite le carte del mazzo, altrimenti non pesco carte.
						*/
						if (m->next<40)
						{
							if ((TMPcard_other= getCard (m))==NULL)
							{
								perror ("server (thread_worker) exit with error on getCard 3");
								err=errno;

								/* Ottengo la mutua esclusione sull'albero degli utenti.
								*/
								pthread_mutex_lock (&mux_tree);
								while (workOnTree)
									pthread_cond_wait (&cond_tree, &mux_tree);

								workOnTree= TRUE;

								/* Se una delle funzioni non termina con successo fallisce il server poiché
									 potrebbe non poter accedere all'AlberoUtenti.
								*/
								if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserStatus 18");
									exit (EXIT_FAILURE);
								}

								if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserChannel 16");
									exit (EXIT_FAILURE);
								}

								/* Rilascio la mutua esclusione, libero memoria e termino il thread.
								*/
								workOnTree= FALSE;

								pthread_cond_signal (&cond_tree);
								pthread_mutex_unlock (&mux_tree);

								free (msg_main.buffer);
								free (msg_other.buffer);
								freeMazzo (m);

								closeConnection (FD_main);
								closeConnection (FD_other);

								if ((pthread_mutex_trylock (&tmp->detached))==0)
									pthread_detach (tmp->PID);
								return (void *) err;
							}

							cardToString (TMPstrcard_other, TMPcard_other);
							sprintf (msg_other.buffer, "t:%s", TMPstrcard_other);
							msg_other.type= MSG_CARD;
							msg_other.length= strlen (msg_other.buffer);
							free (TMPcard_other);
							TMPcard_other=NULL;

							if ((TMPcard_main= getCard (m))==NULL)
							{
								perror ("server (thread_worker) exit with error on getCard 4");
								err=errno;

								/* Ottengo la mutua esclusione sull'albero degli utenti.
								*/
								pthread_mutex_lock (&mux_tree);
								while (workOnTree)
									pthread_cond_wait (&cond_tree, &mux_tree);

								workOnTree= TRUE;

								/* Se una delle funzioni non termina con successo fallisce il server poiché
									 potrebbe non poter accedere all'AlberoUtenti.
								*/
								if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserStatus 19");
									exit (EXIT_FAILURE);
								}

								if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
								{
									perror ("server (thread_worker) crashed on setUserChannel 17");
									exit (EXIT_FAILURE);
								}

								/* Rilascio la mutua esclusione, libero memoria e termino il thread.
								*/
								workOnTree= FALSE;

								pthread_cond_signal (&cond_tree);
								pthread_mutex_unlock (&mux_tree);

								free (msg_main.buffer);
								free (msg_other.buffer);
								freeMazzo (m);

								closeConnection (FD_main);
								closeConnection (FD_other);

								if ((pthread_mutex_trylock (&tmp->detached))==0)
									pthread_detach (tmp->PID);
								return (void *) err;
							}

							cardToString (TMPstrcard_main, TMPcard_main);
							sprintf (msg_main.buffer, "a:%s", TMPstrcard_main);
							msg_main.type= MSG_CARD;
							msg_main.length= strlen (msg_main.buffer);
							free (TMPcard_main);
							TMPcard_main=NULL;
						}

						else
						{
							sprintf (msg_other.buffer, "t");
							msg_other.type= MSG_CARD;
							msg_other.length= strlen (msg_other.buffer);

							sprintf (msg_main.buffer, "a");
							msg_main.type= MSG_CARD;
							msg_main.length= strlen (msg_other.buffer);
						}
					}

					if (((sendMessage (FD_main, &msg_main))!=((msg_main.length)+4+1)) || ((sendMessage (FD_other, &msg_other))!=((msg_other.length)+4+1)))
					{
						perror ("server (thread_worker) exit with error on sendMessage 12");
						err=errno;

						/* Ottengo la mutua esclusione sull'albero degli utenti.
						*/
						pthread_mutex_lock (&mux_tree);
						while (workOnTree)
							pthread_cond_wait (&cond_tree, &mux_tree);

						workOnTree= TRUE;

						/* Se una delle funzioni non termina con successo fallisce il server poiché
							 potrebbe non poter accedere all'AlberoUtenti.
						*/
						if (((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE) ||	((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE))
						{
							perror ("server (thread_worker) crashed on setUserStatus 20");
							exit (EXIT_FAILURE);
						}

						if (((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE) || ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE))
						{
							perror ("server (thread_worker) crashed on setUserChannel 18");
							exit (EXIT_FAILURE);
						}

						/* Rilascio la mutua esclusione, libero memoria e termino il thread.
						*/
						workOnTree= FALSE;

						pthread_cond_signal (&cond_tree);
						pthread_mutex_unlock (&mux_tree);

						free (msg_main.buffer);
						free (msg_other.buffer);
						freeMazzo (m);

						closeConnection (FD_main);
						closeConnection (FD_other);

						if ((pthread_mutex_trylock (&tmp->detached))==0)
							pthread_detach (tmp->PID);
						return (void *) err;
					}
					
					/* Aggiorno gli indici degli array.
					*/
					i--; j++;
				}


				/* La partita è finita!!
					 Setto gli stati degli utenti e i relativi canali.
					 Ottengo la mutua esclusione sull'albero degli utenti.
				*/
				pthread_mutex_lock (&mux_tree);
				while (workOnTree)
					pthread_cond_wait (&cond_tree, &mux_tree);

				workOnTree= TRUE;

				/* Se una delle funzioni non termina con successo fallisce il server poiché
					 potrebbe non poter accedere all'AlberoUtenti.
				*/
				if ((setUserStatus (AlberoUtenti, mainUser, DISCONNECTED))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserStatus 21");
					exit (EXIT_FAILURE);
				}
				if ((setUserStatus (AlberoUtenti, otherUser, DISCONNECTED))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserStatus 22");
					exit (EXIT_FAILURE);
				}

				if ((setUserChannel (AlberoUtenti, mainUser, -1))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserChannel 19");
					exit (EXIT_FAILURE);
				}
				if ((setUserChannel (AlberoUtenti, otherUser, -1))==FALSE)
				{
					perror ("server (thread_worker) crashed on setUserChannel 20");
					exit (EXIT_FAILURE);
				}

				/* Rilascio la mutua esclusione e libero memoria.
				*/
				workOnTree= FALSE;

				pthread_cond_signal (&cond_tree);
				pthread_mutex_unlock (&mux_tree);

				free (msg_main.buffer);
				free (msg_other.buffer);
			}

			/* Calcolo i punti complessivi dei giocatori e determino il vincitore.
				 Invio il messaggio con il vincitore ai client e termino.
			*/

			if ((point_main=computePoints (carte_prese_main, indice_main))==-1)
			{
				perror ("server (thread_worker) exit with error on computePoints");
				err=errno;

				freeMazzo (m);

				closeConnection (FD_main);
				closeConnection (FD_other);

				if ((pthread_mutex_trylock (&tmp->detached))==0)
					pthread_detach (tmp->PID);
				return (void *) err;
			}

			point_other= MAX_SCORE-point_main;

			/* Poiché il messaggio è lo stesso per tutti e due i giocatori ne uso uno solo.
				 Stampo anche sul file di log il vincitore e i punti
			*/
			if ((msg.buffer= malloc (LUSER+1+4+1))==NULL)
			{
				perror ("server (thread_worker) crashed on malloc 16");
				exit (EXIT_FAILURE);
			}

			msg.type= MSG_ENDGAME;

			if (point_main>point_other)
			{
				if ((sprintf (msg.buffer, "%s:%d", mainUser, point_main))<0)
				{
					perror ("server (thread_worker) exit with error on sprintf 0");
					err=errno;

					free (msg.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}

				if ((fprintf (fp, "WINS:%s\nPOINTS:%d\n", mainUser, point_main))<0)
				{
					perror ("server (thread_worker) exit with error on fprintf 5");
					err=errno;

					free (msg.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}
			}

			else if (point_other>point_main)
			{
				if ((sprintf (msg.buffer, "%s:%d", otherUser, point_other))<0)
				{
					perror ("server (thread_worker) exit with error on sprintf 1");
					err=errno;

					free (msg.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}

				if ((fprintf (fp, "WINS:%s\nPOINTS:%d\n", otherUser, point_other))<0)
				{
					perror ("server (thread_worker) exit with error on fprintf 6");
					err=errno;

					free (msg.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}

			}

			else
			{
				if ((sprintf (msg.buffer, "Parità!"))<0)
				{
					perror ("server (thread_worker) exit with error on sprintf 2");
					err=errno;

					free (msg.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}

				if ((fprintf (fp, "Parità\n"))<0)
				{
					perror ("server (thread_worker) exit with error on fprintf 7");
					err=errno;

					free (msg.buffer);
					freeMazzo (m);

					closeConnection (FD_main);
					closeConnection (FD_other);

					return (void *) err;
				}
			}


			msg.length= strlen (msg.buffer);

			if (((sendMessage (FD_main, &msg))!= (msg.length+1+4)) || ((sendMessage (FD_other, &msg))!= (msg.length+1+4)))
			{
				perror ("server (thread_worker) exit with error on sendMessage 13");
				err=errno;

				free (msg.buffer);
				freeMazzo (m);

				closeConnection (FD_main);
				closeConnection (FD_other);

				if ((pthread_mutex_trylock (&tmp->detached))==0)
					pthread_detach (tmp->PID);
				return (void *) err;
			}

			/* Chiudo il file di log.
			*/
			fclose (fp);
		}

		/* Chiudo le connessioni.
		*/
		closeConnection (FD_main);
		closeConnection (FD_other);
	}

	/* Libero tutti dati allocati dinamicamente.
	*/
	for (i=0;i<indice_main;i++)
		free (carte_prese_main[i]);
	freeMazzo (m);
	free (msg.buffer);


	if ((pthread_mutex_trylock (&tmp->detached))==0)
		pthread_detach (tmp->PID);

	return (void *) EXIT_SUCCESS;
}
