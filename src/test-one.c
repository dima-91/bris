/**
   \file
   \author lso13
   
   \brief Test primo frammento
 */

#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
#include <errno.h>
#include <time.h>
#include "bris.h"

/** lunghezza in caratteri della rappresentazione di una carta */
#define LPCARD 2

/** nome file di output per il test */
#define FILETEST1 "./mazzo1.out"

/** carte da confrontare gruppo A */
char* cardA[] =  {
  "JP",
  "3P",
  "QC",
  "KF",
  "7Q",
  "3Q",
  "QQ",
  "2P",
  "JF",
  "AQ",
  "5P",
  "AC",
  "JQ",
  "QP",
  "6F",
  "JC",
  "4Q",
  "5F",
  "7F",
  "4P",
  NULL,
};

/** carte da confrontare gruppo B */
char* cardB[] =  {
  "KP",
  "6Q",
  "4C",
  "AF",
  "5C",
  "2C",
  "7P",
  "7C",
  "KQ",
  "2Q",
  "2F",
  "3F",
  "AP",
  "4F",
  "QF",
  "3C",
  "KC",
  "5Q",
  "6P",
  "6C",
  NULL,
};

/** main di test */
int main (void) {
  mazzo_t * m;
  carta_t *pc, *pd;
  int i;
  char buf[LPCARD+1];
  FILE * fsa;
  carta_t *preseA[NCARTE], *preseB[NCARTE];
  int iA, iB;
  int tot;

  mtrace();

  /* test creazione mazzo nuovo*/
  srand(time(NULL));
  m=newMazzo();
  
  /* stampa del mazzo */
  printMazzo(stdout,m);
  
  freeMazzo(m);

  /* test creazione mazzo nuovo*/
  srand(42);
  m=newMazzo();

  /* stampa del mazzo */
  printMazzo(stdout,m);

  /* 
     stampa su file una carta alla volta 
     pescando le carte dal mazzo 
  */
  
  fprintf(stdout,"Registro le carte del mazzo su file.\n");

  fsa=fopen(FILETEST1,"w");
  if ( fsa == NULL ) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  i = 0;

  while ( (pc = getCard(m)) != NULL ){
    carta_t* tmp;

    /* numero progressivo carta */
    fprintf(fsa,"%2d ",i);

    /* trasformo in stringa */
    cardToString(buf,pc);

    /* ri-trasformo in carta */
    if ( ( tmp=stringToCard(buf) ) == NULL ) {
      perror("stringToCard:");
      exit(EXIT_FAILURE);
    }

    /* ri-trasformo in stringa (test deve ritornare la stessa cosa) */
    cardToString(buf,tmp);
    free(tmp);

    /* stampo su file */
    fprintf(fsa,"%s\n",buf);
    free(pc);

    i++;
  }



  /* errori nella getCard ? */
  if ( pc == NULL && errno != 0 ){
    perror("getCard:");
    exit(EXIT_FAILURE);
  }


  /* sono state lette tutte le carte ? */
  if ( i != NCARTE ) {
    fprintf(stderr,"getCard: Lettura scorretta\n");
    exit(EXIT_FAILURE);
  }

  /* test funzioni di confronto e di somma punti */
  /* inizializzo array prese */
  iA=iB=0;
  for (i =0; i< NCARTE; i++ ) 
    preseA[i]=preseB[i]=NULL;

  /* briscola CUORI */
  for (i = 0; cardA[i] != NULL; i++) {

    /* trasformo le carte da stringa ...*/
    if ( ( pc = stringToCard(cardA[i]) ) == NULL  ||  ( pd = stringToCard(cardB[i]) ) == NULL ) {
       perror("stringToCard:");
       exit(EXIT_FAILURE);
    }

    /* confronto */
    if ( compareCard(CUORI,pc,pd) == TRUE ) {
      fprintf(fsa,"%s %s --> A\n",cardA[i],cardB[i]);
      preseA[iA]=pc;
      preseA[iA+1]=pd;
      iA+=2; 
    }
    else {
      fprintf(fsa,"%s %s --> B\n",cardA[i],cardB[i]);
      preseB[iB]=pc;
      preseB[iB+1]=pd;
      iB+=2; 
    }  
  }



  /* calcolo e stampo i punti delle prese */
  tot=computePoints(preseA,iA);
  fprintf(fsa,"Punti %d preseA=%d\n",iA, tot);

  tot=computePoints(preseB,iB);
  fprintf(fsa,"Punti %d preseB=%d\n",iB, tot);

  /* dealloco */
  for (i = 0; i< NCARTE; i ++ ) {
    if ( preseA[i] != NULL ) free(preseA[i]) ;
    if ( preseB[i] != NULL ) free(preseB[i]) ;
  }

  /* cleanup */
  freeMazzo(m);
  fclose(fsa);
  muntrace();

  return EXIT_SUCCESS;
}
