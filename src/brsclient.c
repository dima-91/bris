/**
   \file brsclient.c
   \author Luca Di Mauro.
   \brief Codice client progetto bris.
   
   Si dichiara che il contenuto di questo file e' in ogni sua parte opera
	 originale dell' autore.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comsock.h"
#include "bris.h"
#include "users.h"
#include "brsservcli.h"
#include <errno.h>


/** Fuzione main del programma.
	* La funzione controlla che gli argomenti in ingresso siano esatti e successivamente guarda se
	* è stato attivato con l'opzione -r o -c, rispettivamente registrazione e cancellazione,
	* e quindi esegue le operazioni necessarie, oppure l'utente che lo ha attivato vuole connettersi
	* al server e fare una partita, e quindi si connette al server ed esegue le operazioni di
	* autenticazione, scelta dell'avversario e gestione della partita.
*/
int main (int argn, char **arg)
{
	/* Indici di controllo. */
	int i=0, j=0;
	/*	Intero per la lunghezza del buffer. */
	int n=0;
	/*	File descriptor della socket. */
	int FD=0;
	/* Struttura per l'invio e la ricezione dei messaggi. */
	message_t msg;
	/* Stringa per identificare il giocatore sfidante o sfidato. */
	char challengingUser[LUSER+1];
	/* Carattere per identificare il seme della briscola. */
	char briscola;
	/* Array per identificare con stringhe le carte in mano del giocatore. */
	char *carte[3];
	/* Variabile per indicare se l'utente è rimasto o no in attesa di uno sfidante. */
	bool_t waiting=FALSE;
	/* Stringa per memorizzare la carta giocata dall'utente. */
	char card_played[3];
	/* Variaibile per verificare che una carta appartenga alla mano del giocatore. */
	bool_t check_card=FALSE;

	/* Controllo che il numero di argomenti sia o 3 o 4 e che il nome utente e la password
		 non superino la lunghezza massima.
	*/
	if (argn>4 || argn<3 || strlen (arg[1])>20 || strlen (arg[2])>8)
	{
		errno=EINVAL;
		perror ("client crashed on check arguments 0");
		exit (EXIT_FAILURE);
	}



	/* Il programma è stato invocato con opzione -r o -c.
		 Controllo con quale delle due opzioni è stato chiamato il programma
		 e assegno a msg.type il valore r o c.
	*/
	if (argn==4)
	{
		if (strcmp (arg[3], "-r")==0)
		{
			msg.type= MSG_REG;
		}
		else if (strcmp (arg[3], "-c")==0)
		{
			msg.type= MSG_CANC;
		}
		else
		{
			errno=EINVAL;
			perror ("client crashed on check arguments 1");
			exit (EXIT_FAILURE);
		}

		/* Apro una connessione con il server, preparo il messaggio
			 e gli invio il messaggio di registrazione o cancellazione.
		*/
		if ((FD= openConnection (BRSSOCKET, MAXTRIAL-1, 1))==-1)
		{
			perror ("clientcrashed on openConnection0");
			exit (EXIT_FAILURE);
		}

		n= (strlen (arg[1]))+(strlen (arg[2]))+2;
		if ((msg.buffer= (char *) malloc ((n+1)*sizeof(char)))==NULL)
		{
			perror ("client crashed on malloc0");
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}

		snprintf (msg.buffer, n, "%s:%s", arg[1], arg[2]);
		msg.length= strlen (msg.buffer);

		if ((sendMessage (FD, &msg))!= ((strlen (msg.buffer))+1+4))
		{
			perror ("client crashed on sendMessage0");
			free (msg.buffer);
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}

		free (msg.buffer);
		msg.buffer=NULL;

		/* Ricevo l'esito dell'operaione, stampo il messaggio sullo standard output
			 e termino il programma.
		*/
		if ((receiveMessage (FD, &msg))==-1)
		{
			perror ("client crashed on receiveMessage0");
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}

		if (msg.type== MSG_OK)
		{
			printf ("**Server says: Operazione riuscita**\n");
		}
		else if (msg.type== MSG_NO || msg.type== MSG_ERR)
		{
			printf ("**Server says: %s**\n", msg.buffer);
		}

		free (msg.buffer);
		closeConnection (FD);

		exit (EXIT_SUCCESS);
	}

	/* Il client non è stato avviato con opzione -r o -c e quindi vuole iniziare una partita.
		 Mi connetto con il server, preparo il messagio da inviare, lo invio e attendo la risposta.
	*/
	else
	{
		if ((FD= openConnection (BRSSOCKET, MAXTRIAL-1, 2))==-1)
		{
			perror ("client crashed on openConnection1");
			exit (EXIT_FAILURE);
		}

		n= (strlen (arg[1]))+(strlen (arg[2]))+2;
		if ((msg.buffer= (char *) malloc ((n+1)))==NULL)
		{
			perror ("client crashed on malloc1");
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}
		snprintf (msg.buffer, n, "%s:%s", arg[1], arg[2]);
		msg.length= strlen (msg.buffer);
		msg.type= MSG_CONNECT;


		/* Mando il messaggio per connettermi al sevizio.
		*/
		if ((sendMessage (FD, &msg))!= ((strlen (msg.buffer))+1+4))
		{
			perror ("client crashed on sendMessage1");
			free (msg.buffer);
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}

		free (msg.buffer);
		msg.buffer=NULL;

		/* Ricevo il messaggio e controllo l'esito.
		*/
		if ((receiveMessage (FD, &msg))==-1)
		{
			perror ("client crashed on receiveMessage1");
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}

		if (msg.type== MSG_NO || msg.type== MSG_ERR)
		{
			/* La conessione è andata male. Chiudo il canale di connessione
				 e termino il programma.
			*/
			printf ("**Server says: %s**\n", msg.buffer);
			free (msg.buffer);
			closeConnection (FD);
			return EXIT_SUCCESS;
		}

		/* Non ci sono avversari disponibili. Lo segnalo all'utente.
		*/
		else if (msg.type== MSG_WAIT)
		{
			printf ("Non ci sono avversari disponibili.\nIn attesa di un avversario.\n");
			waiting=TRUE;
		}

		/* Mi sono connesso con successo e ci sono avversari disponibili per una partita.
			 Stampo il messaggio che mi arriva con la lista degli avversari e attendo
			 la scelta dell'utente.
		*/
		else if (msg.type== MSG_OK)
		{
			printf ("Possibili avversari: %s\nWAIT per attendere una sfida\nScelta --?\n", msg.buffer);
			free (msg.buffer);
			if ((msg.buffer= malloc (LUSER))==NULL)
			{
				perror ("client crashed on malloc2");
				closeConnection (FD);
				exit (EXIT_FAILURE);
			}
			scanf ("%s", msg.buffer);

			if ((strcmp (msg.buffer, "WAIT"))==0)
			{
				/* L'utente ha deciso di aspettare per essere sfidarto.
				*/
				waiting=TRUE;
				msg.type= MSG_WAIT;
			}

			else
			{
				/* L'utente ha scelto chi sfidare.
				*/
				msg.type= MSG_OK;
			}

			/* Invio il messaggio con la scelta effettuata al server.
			*/
			if ((sendMessage (FD, &msg))!= ((msg.length)+4+1))
			{
				perror ("client crashed on sendMessage2");
				free (msg.buffer);
				closeConnection (FD);
				exit (EXIT_FAILURE);
			}
		}
		
		free (msg.buffer);
		

		/* Arrivato a questo punto sto aspettando uno sfidante, oppure ho scelto uno sfidante.
			In entrambi i casi aspetto un messaggio di inizio partita da parte del server.
		*/
		if ((receiveMessage (FD, &msg))==-1)
		{
			perror ("client crashed on receiveMessage2");
			closeConnection (FD);
			exit (EXIT_FAILURE);
		}

		if (msg.type==MSG_NO)
		{
			printf ("**Server says: %s**\n", msg.buffer);
			free (msg.buffer);
			closeConnection (FD);
			return EXIT_SUCCESS;
		}

		else if (msg.type==MSG_STARTGAME)
		{
			/* Ho ricevuto il messaggio che mi dice che briscola è, il nome dell'avversario e la prima mano di carte.
				 Scompatto il messaggio e lo stampo sullo stdout.
			*/
			i=0;
			while (msg.buffer[i]!=':')
			{
				challengingUser[i]=msg.buffer[i];
				i++;
			}
			challengingUser[i]='\0';
			i++;
			briscola= msg.buffer[i];
			i+=2;

			for (j=0;j<3;j++)
				if ((carte[j]= malloc (3))==NULL)
				{
					perror ("client crashed on malloc3");
					closeConnection (FD);
					exit (EXIT_FAILURE);
				}

			for (j=0;j<3;j++)
			{
				carte[j][0]=msg.buffer[i];
				i++;
				carte[j][1]=msg.buffer[i];
				i++;
				carte[j][2]='\0';
				i++;
			}
			free (msg.buffer);

			printf ("Giochiamo con %s\nBriscola: %c\n", challengingUser, briscola);
			bzero (card_played, sizeof (card_played));
			i=N_TURNI;
			while (i)
			{
				/* Se non siamo nelle ultime due mani stampo tutte le carte, altrimenti solo quelle che sono diverse da NULL.
				*/
				if (i>2)
				{
					printf ("Carte %s %s %s\n", carte[0], carte [1], carte[2]);
				}
				else
				{
					printf ("Carte");
					if (carte[0]!=NULL)
						printf (" %s", carte[0]);
					if (carte[1]!=NULL)
						printf (" %s", carte[1]);
					if (carte[2]!=NULL)
						printf (" %s", carte[2]);
					printf ("\n");
				}

				if (waiting==FALSE)
				{
					/* Tocca all'utente di questo client. Attendo che l'utente inserisca la carta da giocare.
					*/
					printf ("Turno %s --? ", arg[1]);
					if ((msg.buffer= malloc (3))==NULL)
					{
						perror ("client crashed on malloc4");
						closeConnection (FD);
						exit (EXIT_FAILURE);
					}

					scanf ("%s", msg.buffer);

					/* Controllo che il giocatore abbia giocato una carta giusta.
					*/
					check_card= FALSE;
					while (!check_card)
					{
						if (carte[0]!=NULL && strcmp (msg.buffer, carte[0])==0)
						{
							check_card=TRUE;
							break ;
						}
						if (carte[1]!=NULL && strcmp (msg.buffer, carte[1])==0)
						{
							check_card= TRUE;
							break ;
						}
						if (carte[2]!=NULL && strcmp (msg.buffer, carte[2])==0)
						{
							check_card= TRUE;
							break ;
						}
						printf ("Gioca una carta tra quelle presenti in mano!\nTurno %s --? ", arg[1]);
						scanf ("%s", msg.buffer);
					}

					/* L'utente ha giocato una carta presente nella sua mano.
						 Invio il messaggio al server con la carta giocata.
					*/
					msg.type= MSG_PLAY;
					msg.length= strlen (msg.buffer);

					if ((sendMessage (FD, &msg))!=((msg.length)+1+4))
					{
						perror ("client crashed on sendMessage3");
						free (msg.buffer);
						closeConnection (FD);
						exit (EXIT_FAILURE);
					}

					/* Salvo in card_played la carta appena giocata per poterla eliminare successivamente.
					*/
					strcpy (card_played, msg.buffer);
					free (msg.buffer);

					/* Aspetto il messaggio di risposta che mi indica la carta giocata dall'avversario.
					*/
					printf ("Turno %s --> ", challengingUser);
					fflush (NULL);
					if ((receiveMessage (FD, &msg))==-1)
					{
						perror ("client crashed on receiveMessage3");
						closeConnection (FD);
						exit (EXIT_FAILURE);
					}
					printf ("%s\n", msg.buffer);
					free (msg.buffer);
				}

				else
				{
					/* Tocca allo sfidante. Aspetto il messaggio che mi indica la carta giocata dall'avversario.
					*/
					printf ("Turno %s --> ", challengingUser);
					fflush (NULL);
					if ((receiveMessage (FD, &msg))==-1)
					{
						perror ("client crashed on receiveMessage4");
						closeConnection (FD);
						exit (EXIT_FAILURE);
					}
					printf ("%s\n", msg.buffer);
					free (msg.buffer);

					printf ("Turno %s --? ", arg[1]);
					if ((msg.buffer= malloc (3))==NULL)
					{
						perror ("client crashed on malloc5");
						closeConnection (FD);
						exit (EXIT_FAILURE);
					}

					/* Attendo che l'utente inserisca la carta da giocare.
					*/
					scanf ("%s", msg.buffer);

					/* Controllo che il giocatore abbia giocato una carta giusta.
					*/
					check_card= FALSE;
					while (!check_card)
					{
						if (carte[0]!=NULL && strcmp (msg.buffer, carte[0])==0)
						{
							check_card=TRUE;
							break ;
						}
						if (carte[1]!=NULL && strcmp (msg.buffer, carte[1])==0)
						{
							check_card= TRUE;
							break ;
						}
						if (carte[2]!=NULL && strcmp (msg.buffer, carte[2])==0)
						{
							check_card= TRUE;
							break ;
						}
						printf ("Gioca una carta tra quelle presenti in mano!\nTurno %s --? ", arg[1]);
						scanf ("%s", msg.buffer);
					}

					/* L'utente ha giocato una carta presente nella sua mano.
						 Invio il messaggio al server con la carta giocata.
					*/
					msg.type= MSG_PLAY;
					msg.length= strlen (msg.buffer);
					if ((sendMessage (FD, &msg))!= ((msg.length)+1+4))
					{
						perror ("client crashed on sendMessage4");
						free (msg.buffer);
						closeConnection (FD);
						exit (EXIT_FAILURE);
					}

					/* Salvo in card_played la carta appena giocata per poterla eliminare successivamente.
					*/
					strcpy (card_played, msg.buffer);
					free (msg.buffer);
				}


				/* Ricevo il messaggio che mi dice la carta pescata e a chi tocca giocare per primo.
					 Setto la variabile waiting e sostituisco la carta giocata con la nuova.
				*/
				if ((receiveMessage (FD, &msg))==-1)
				{
					perror ("client crashed on receiveMessage5");
					closeConnection (FD);
					exit (EXIT_FAILURE);
				}
				if (msg.buffer[0]=='a')
					waiting=TRUE;
				else
					waiting=FALSE;

				/* Cerco la carta da eliminare e la sostituisco.
					 Se non sono all'ultima mano procedo normalmente, altrimenti elimino direttamente
					 tutte le carte perché la partita è finita.
				*/
				j=0;
				if (i!=1)
				{
					while (j<3)
					{
						if (carte[j]!=NULL)
						{
							if ((strcmp(card_played, carte[j]))!=0)
								j++;
							else
								break ;
						}
						else
							j++;
					}
				}

				else
				{
					for (j=0;j<3;j++)
					{
						free (carte[j]);
						carte[j]=NULL;
					}
				}

				/* Se non sono nelle ultime 3 mani sostituisco la carta giocata con quella pesscata.
				*/
				if (i>3)
				{
					carte[j][0]=msg.buffer[2];
					carte[j][1]=msg.buffer[3];
				}

				/* Se invece sono nella terzultima o nella penultima mano elimino la carta giocata.
				*/
				else if (i==2 || i==3)
				{
					free (carte[j]);
					carte[j]=NULL;
				}

				/* Aggiorno l'indice delle mani.
				*/
				i--;
			}

			/* È finita la partita.
				 Ricevo il messaggio che mi dice chi è il vincitore, lo stampo sullo stdout e termino il programma.
			*/

			if ((receiveMessage (FD, &msg))==-1)
			{
				perror ("client crashed on receiveMessage6");
				closeConnection (FD);
				exit (EXIT_FAILURE);
			}

			if ((strcmp (msg.buffer, "Parità!"))==0)
				printf ("Parità!\nBye");

			else
			{
				j=0;
				printf ("Vince ");

				while (msg.buffer[j]!=':')
				{
					printf ("%c", msg.buffer[j]);
					j++;
				}

				printf (" con ");
				j++;

				while (msg.buffer[j]!='\0')
				{
					printf ("%c", msg.buffer[j]);
					j++;
				}

				printf (" punti!\nBye");
			}

      free (msg.buffer);
		}
	}
	return EXIT_SUCCESS;
}
