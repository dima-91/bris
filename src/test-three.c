/**   \file
      \author lso13
   
      \brief Test primo frammento
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mcheck.h>

#include "users.h"


/** nome file di test */
#define FILETEST2 "./users1.txt"
#define FILETEST3 "./users2.txt"

/** utenti per test inserimento gia' registrati */
char* moreUsers[] = {
  "plutoXXX:baubau",
  "superpippo:sssuuppp",
  NULL,
};

/** nomi utenti per test sulle funzioni che lavorano sullo stato e sul canale */
char* statusUsers[] = {
  "pluto",
  "superpippo",
  "paperoga",
  "minni",
  "topolino",
  "paperino",
  NULL,
};

/** main di test */
int main (void) {
  int cont;
  nodo_t* root=NULL;
  FILE* fsa;
  user_t* us;
  int i,err;
  char * userlist;

  /* controllo memoria */
  mtrace();

  /* test di loadUsers */
  fprintf(stdout,"Lettura utenti da file.\n");
  fsa=fopen(FILETEST2,"r");
  if ( (cont = loadUsers(fsa,&root) ) == -1 ) {
    perror("loadUsers: Errore fatale");
    exit(EXIT_FAILURE);
  }
  
  fprintf(stdout,"Letti %d utenti.\n",cont);
  fclose(fsa);

  /* inseriamo utenti gia' registrati */
  for(i=0;moreUsers[i]!=NULL;i++) {
    if ( (us = stringToUser(moreUsers[i],strlen(moreUsers[i])+1)) == NULL ) {
      perror("stringToUser: Errore fatale");
      exit(EXIT_FAILURE);
    }
    fprintf(stderr,"Aggiungo %s\n",moreUsers[i]);

    if ( ( err = addUser(&root,us) ) != 1 )  {
      perror("addUser: Errore fatale");
      free(us);
      exit(EXIT_FAILURE);
    }
    free(us);
  }

  /* test per isUser, getUserStatus, getUserChannel, setUserStatus, 
     setUserChannel e getuserList*/

  /* controlliamo che gli utenti siano registrati correttamente e 
     inseriamo lo stato WAITING in alcuni di loro */
  for(i=0;statusUsers[i]!=NULL;i++)  {

    if ( !isUser(root,statusUsers[i]) )  {
      fprintf(stderr,"isUser: Errore fatale %s\n",statusUsers[i]);
      exit(EXIT_FAILURE);
    }
    setUserStatus(root,statusUsers[i],WAITING);
    setUserChannel(root,statusUsers[i],i);
  }
  /* stampa albero su stdout */
  printTree(root);
  
  /* controlliamo lo stato WAITING  appena settato e il channel */  
  for(i=0;statusUsers[i]!=NULL;i++) {
    if ( getUserStatus(root,statusUsers[i]) != WAITING ) {
      fprintf(stderr,"getUserStatus: Errore fatale %s\n",statusUsers[i]);
      exit(EXIT_FAILURE);
    }
    if ( getUserChannel(root,statusUsers[i]) != i ) {
      fprintf(stderr,"getUserChannel: Errore fatale %s\n",statusUsers[i]);
      exit(EXIT_FAILURE);
    }
  } 
   
  /* creiamo la lista degli utenti WAITING e verifichiamo sia giusta 
     ... con un ciclo di strstr */
  if ( ( userlist=getUserList(root,WAITING) ) ==  NULL ) {
    fprintf(stderr,"getUserList: Errore fatale\n");
    exit(EXIT_FAILURE);
  }
  
  fprintf(stdout,"Lista utenti WAITING <%s>\n",userlist);
  for(i=0;statusUsers[i]!=NULL;i++) {
    if ( strstr(userlist,statusUsers[i]) == NULL ) {
      fprintf(stderr,"getUserList: Errore fatale: stringa %s non presente\n",statusUsers[i]);
      exit(EXIT_FAILURE);
    }
  }
  free(userlist);

  /* registrazione utenti su file */
  fprintf(stdout,"Registro gli utenti su file.\n");
  fsa=fopen(FILETEST3,"w");
  if ( ( fsa == NULL ) || ( cont != storeUsers(fsa,root) ) ) {
    perror("storeUsers: Errore fatale");
    exit(EXIT_FAILURE);
  }
  fprintf(stdout,"Registrati %d utenti.\n",cont);
  fclose(fsa);
  
  /* cleanup */
  freeTree(root);
  muntrace();
  fprintf(stdout,"Test Superato!!\n");
  return EXIT_SUCCESS; 
}

