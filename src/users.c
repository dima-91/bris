/** \file users.c
		\author Luca Di Mauro.
		\brief Implementazione delle funzioni definite in users.h.
		
		Si dichiara che il contenuto di questo file e' in ogni sua parte opera
		originale dell' autore.
*/
#include "users.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>




/** \brief La funzione converte una stringa "r", passata come parametro, in una
	* struttura utente ::user_t, allocando memoria per quest'ultima.

	* Dopo aver controllato la correttezza degli argomenti, copia nome utente
	* e password nei rispettivi campi della struttura ::user_t, controllando
	* che la lunghezza di questi ultimi non superi quella massima.
*/
user_t *stringToUser (char* r, unsigned int l)
{
	user_t *new=NULL;
	int i=0, j=0;

	/* Se la stringa è uguale a NULL o se il numero dei caratteri che si possono leggere è minore
    della lunghezza della stringa o se non esiste il carattere ":" che divide il nome utente
		 dalla password, setto errno a EINVAL e restituisco NULL.
	*/
	if (r==NULL || strlen (r)>l || (strchr(r, ':')==NULL))
		{
			errno=EINVAL;
			return NULL;
		}
	if ((new= (user_t *) malloc (sizeof(user_t)))==NULL)
	 return NULL;

	/* Copio il nome utente contenuto nella stringa "r" nel campo "name"
		 della struttura utente.
	*/
	while (r[i]!=':' && i<=LUSER)
	{
		new->name[i]=r[i];
		i++;
	}

	/* Controllo che il numero di caratteri scritti, contenuti nella variabile "i",
		 per il nome utente non superi la sua lunghezza massima e che sia maggiore di zero,
		 altrimenti restituisco NULL.
	*/
	if (i>LUSER || i==0)
	  {
	    free (new);
	    errno=EINVAL;
	    return NULL;
	  }
	else
	  new->name[i]='\0';
	  
	/* Incremento la variabile i e copio la password contenuta nella stringa r, a partire dalla posizione i,
		 nel campo "passwd" della struttura utente.
	*/
	i++;
	while (r[i]!='\0' && j<=LPWD)
	{
		new->passwd[j]=r[i];
		i++; j++;
	}

	/* Controllo che il numero di caratteri scritti, contenuti nella variabile "j",
		 per la password non superi la lunghezza massima e che sia maggiore di zero,
		 altrimenti restituisco NULL.
	*/
	if (j>LPWD || j==0)
	  {
	    free (new);
	    errno=EINVAL;
	    return NULL;
	  }
	else
	  new->passwd[j]='\0';

	return new;
}




/**	\brief La funzione converte la struttura ::user_t "puser", passata come parametro,
	* in una stringa allocando memoria per la stringa.

	* Dopo aver controllato la correttezza degli argomenti, alloca memoria per
	* la stringa e vi copia il nome utente e la password nei rispettivi campi.		
*/
char *userToString (user_t *puser)
{
	char *new=NULL;
	int l=0, i=0, j=0;

	/* Controllo che "puser" contenga sia il nome che il cognome, altrimenti setto errno a EINVAL e restituisco NULL.
	*/
	if (puser->name==NULL || puser->passwd==NULL)
		{
			errno=EINVAL;
			return NULL;
		}

	/* Definisco la lunghezza della tringa come la lunghezza del nome dell'utente più
		 la lunghezza della password più due caratteri per il carattere ':' e il carattere
		 di fine stringa.
	*/
	l= 2 + (strlen (puser->name)) + (strlen (puser->passwd));
	if ((new= (char *) malloc (l*sizeof(char)))==NULL)
		return NULL;

	/* Copio nella stringa il nome dell'utente terminato dal carattere ":".
	*/
	while (puser->name[i]!='\0')
	{
		new[j]=puser->name[i];
		i++; j++;
	}
	new[j]=':';
	j++;

	/* Copio nella stringa la password dell'utente, a partire dalla posizione j,
		 terminata dal carattere di fine stringa.
	*/
	i=0;
	while (puser->passwd[i]!='\0')
	{
		new[j]=puser->passwd[i];
		i++; j++;
	}
	new[j]='\0';
	
	return new;
}




/** \brief La funzione stampa ricorsivamente l'albero di utenti "r", passato come parametro, su stdout in ordine lessicografico.

	*	Se "r" è uguale a NULL termino la funzione, altrimenti richiamo la funzione sul ramo sinistro,
	*	poi stampo il nome utente, la password, il canale e lo stato e richiamo la funzione sul ramo destro.
*/
void printTree (nodo_t *r)
{
	if (r==NULL)
		return ;
	printTree (r->left);
	printf ("%s:%s:%d:", r->user->name, r->user->passwd, r->channel);
	switch (r->status)
	{
		case DISCONNECTED:
			printf ("disconnected\n");
			break ;
		case WAITING:
			printf ("waiting\n");
			break ;
		case PLAYING:
			printf ("playing\n");
			break ;
	}
	printTree (r->right);
}




/** \brief La funzione aggiunge un utente "puser", passato come parametro, nell'albero "r", passato come parametro.

	* Dopo aver controllato la correttezza degli argomenti, la funzione inserisce l'utente
	* nell'albero se questo è vuoto, altrimenti scandisce l'albero finché non arriva in fondo,
	* e quindi aggiunge l'utente, o finché non ne trova uno con lo stesso nome, quindi restituisce 1.
*/
int addUser(nodo_t** r, user_t* puser)
{
	nodo_t *padre=NULL, *corr=NULL;

	/* Controllo che "puser" contenga sia il nome che il cognome, altrimenti seto errno a EINVAL e restituisco NULL.
	*/
	if (puser->name==NULL || puser->passwd==NULL)
	{
		errno=EINVAL;
		return -1;
	}

	/* Controllo se l'albero è vuoto. Se è vero creo un nuovo nodo
		 che diventa la radice dell'albero.
	*/
	if (*r==NULL)
	{
		if (((*r)= (nodo_t *) malloc (sizeof(nodo_t)))==NULL)
			return -1;
		(*r)->left=NULL; (*r)->right=NULL;
		(*r)->channel=-1; (*r)->status=0;
		(*r)->user= puser;
		return 0;
	}

	else
	{
		/* Siccome l'albero non è vuoto, scandisco l'albero, con i puntatori "corr" e "prec",
			 finché non arrivo in fondo o finché non trovo un utente uguale a quello che voglio inserire.
		*/
		corr= (*r);
		while (corr!=NULL && strcmp (corr->user->name, puser->name)!=0)
		{
			if (strcmp (corr->user->name, puser->name)<0)
				{ padre=corr; corr=corr->right; }
			else
				{ padre=corr; corr=corr->left; }
		}

		/* Controllo se sono arrivato in fondo all'abero.
		*/
		if (corr==NULL)
		{
			/* Controllo se il nuovo utente va inserito a destra o a sinistra della foglia puntata da "padre".
			*/
			if (strcmp (padre->user->name, puser->name)<0)
				{
					/* Inserisco il nodo alla destra del padre.
					*/
					if ((padre->right= (nodo_t *) malloc (sizeof(nodo_t)))==NULL)
						return -1;
					corr=padre->right;
					corr->user= puser;
					corr->right=NULL; corr->left=NULL;
					corr->channel=-1; corr->status=0;
					return 0;
				}

			else
				{
					/* Inserisco il nodo alla sinistra del padre.
					*/
					if ((padre->left= (nodo_t *) malloc (sizeof(nodo_t)))==NULL)
						return -1;
					corr=padre->left;
					corr->user= puser;
					corr->right=NULL; corr->left=NULL;
					corr->channel=-1; corr->status=0;
					return 0;
				}
		}

		/* L'utente che voglio inserire esiste già.
		*/
		else
			return 1;
	}
}




/** \brief La funzione controlla la password di un utente.

	* Dopo aver verificato la correttezza degli argomenti, scansiona l'albero
	* Finché non arriva in fondo o finché non trova l'utente cercato.
	* Se l'utente esiste e la password corrisponde restituisce TRUE, altrimenti FALSE.
*/
bool_t checkPwd (nodo_t *r, user_t *user)
{
	nodo_t *corr=NULL;

	/* Se l'albero non esiste restituisce FALSE.
	*/
	if (r==NULL)
		return FALSE;


	else
	{
		/* Scansiono l'albero, con il puntatore "corr", finché non arrivo in fondo o non trovo l'utente cercato.
		*/
		corr=r;

		while (corr!=NULL && (strcmp (corr->user->name, user->name)!=0))
		{
			if (strcmp (corr->user->name, user->name)<0)
				corr=corr->right;
			if (strcmp (corr->user->name, user->name)>0)
				corr=corr->left;
		}

		if (corr==NULL || strcmp (corr->user->passwd, user->passwd)!=0)
			return FALSE;
		else
			return TRUE;
	}
}




/** \brief La funzione rimuove un utente "puser", passato come parametro, dall'albero "r", passato come parametro.

	* Dopo aver verificato la correttezza degli argomenti, la funzione cerca l'utente da rimuovere. Se questo
	* è la radice controllo se ha figli e in caso positivo cerco il nodo più a sinistra nel sottoalbero destro
	* per sostituirlo.
	* Se il nodo da rimuovere non è la radice, cerco il nodo nell'albero e, se ha figli cerco nel sottoalbero
	* destro il nodo più a sinistra per sostituirlo.
	* 
*/
int removeUser(nodo_t** r, user_t* puser)
{
	nodo_t *corr=NULL, *padre=NULL, *padre_n=NULL, *corr_n=NULL;

	/* Controllo se l'albero è uguale a NULL oppure l'utente è uguale a NULL
		 oppure il nome dell'utente è uguale a NULL. Se è vero setto errno a 22
		 e restituisco -1.
	*/
	if ((*r)==NULL || puser==NULL || puser->name==NULL)
	{
		errno=22;
		return -1;
	}

	/* Controllo se l'utente da cercare è nella radice dell'albero.
	*/
	if ((strcmp ((*r)->user->name, puser->name))==0)
	{
		/* Controllo se non esiste la password oppure se esiste ed è esatta.
		*/
		if ((puser->passwd==NULL) || (strcmp ((*r)->user->passwd, puser->passwd))==0)
		{
			/* Controllo se la radice dell'albero è una foglia.
				 Se è vero libero la memoria relativa alla radice,
				 assegno ad r NULL e restituisco 0.
			*/
			if ((*r)->left==NULL && (*r)->right==NULL)
			{
				free ((*r)->user);
				free (*r);
				*r=NULL;
				return 0;
			}

			/* Controllo se la radice dell' albero ha solo il figlio sinistro.
				 Se è vero allora libero la memoria relativa alla radice,
				 assegno alla radice dell'albero il figlio e restituisco 0.
			*/
			if ((*r)->left!=NULL && (*r)->right==NULL)
			{
				corr=((*r)->left);
				free ((*r)->user);
				free (*r);
				*r=corr;
				return 0;
			}

			/* Controllo se la radice dell'albero ha solo il figlio destro.
				 Se è vero allora libero la memoria relativa alla radice,
				 assegno alla radice dell'albero il figlio e restituisco 0.
			*/
			else if ((*r)->left==NULL && (*r)->right!=NULL)
			{
				corr=((*r)->right);
				free ((*r)->user);
				free (*r);
				*r=corr;
				return 0;
			}

		/* La radice dell'albero ha entrambi i figli. Cerco nel sottoalbero destro un nodo,
			 memorizzandolo in "corr_n", da spostare in radice.
		*/
			else
			{
				corr_n= (*r)->right; padre_n= (*r);

				/* Cerco il nodo più a sinistra nel sottoalbero destro.
				*/
				while (corr_n->left!=NULL)
				 { padre_n=corr_n; corr_n=corr_n->left; }

				/* Controllo se "cor_n" è una foglia. Se è vero lo sostituisco radice,
					 libero la memoria relativa alla vecchia radice e restituisco 0.
				*/
				if (corr_n->right==NULL)
				{
					padre_n->left=NULL;
					corr_n->left=(*r)->left;
					corr_n->right=(*r)->right;
					free ((*r)->user);
					free (*r);
					*r=corr_n;
					return 0;
				}

				/* Il nodo trovato ha un figlio destro, quindi il figlio destro
					 di "corr_n" diventa il figlio sinistro del padre di "corr_n",
					 "corr_n" viene messo come radice dell'albero, libero la memoria
					 relativa alla vecchia radice e restituisco 0.
				*/
				else
				{
					padre_n->left=corr_n->right;
					corr_n->left=(*r)->left;
					corr_n->right=(*r)->right;
					free ((*r)->user);
					free (*r);
					*r=corr_n;
					return 0;
				}
			}
		}

		else
		 return WRPWD;
	}

	/* La radice non è il nodo che cercavo. quindi scansiono
		 l'albero finché non arrivo fino in fondo o finché non
		 trovo il nodo da eliminare, e lo memorizzo nela variabile "corr".
	*/
	corr=*r;
	while (corr!=NULL && (strcmp (corr->user->name, puser->name))!=0)
	{
		if ((strcmp (corr->user->name, puser->name))<0)
			{ padre=corr; corr=corr->right; }
		else
			{ padre=corr; corr=corr->left; }
	}

	/* Se corr è uguale a NULL, non esiste l'utente da eliminare quindi restituisco NOUSR.
	*/
	if (corr==NULL)
		return NOUSR;

	/* Controllo se non esiste la password oppure esiste ed è esatta.
	*/
	if ((puser->passwd==NULL) || ((strcmp (corr->user->passwd, puser->passwd))==0))
	{
		/* Controllo se il nodo da eliminare è una foglia.
		*/
		if (corr->left==NULL && corr->right==NULL)
		{
			/* Guardo se il nodo da eliminare è a sinistra o a destra del padre,
				 memorizzato nella variabile "padre", in modo da assegnare NULL
				 al puntatore a "corr". Successivamente libero la memoria realitva a "corr".
			*/
			if (strcmp (padre->user->name, puser->name)<0)
			{
				padre->right=NULL;
				free (corr->user);
				free (corr);
				return 0;
			}
			else
			{
				padre->left=NULL;
				free (corr->user);
				free (corr);
				return 0;
			}
		}

		/* Controllo se il nodo da eliminare ha solo il figlio destro.
		*/
		else if (corr->left==NULL && corr->right!=NULL)
		{
			/* Guardo se "corr" è a destra o a sinistra del padre, memorizzato nella variabile "padre",
				 in modo da far diventare il figlio destro di "corr", figlio destro o sinistro di "padre".
				 Successivamente libero la memoria realitva a "corr".
			*/
			if (strcmp (padre->user->name, puser->name)<0)
			{
				padre->right=corr->right;
				free (corr->user);
				free (corr);
				return 0;
			}
			else
			{
				padre->left=corr->right;
				free (corr->user);
				free (corr);
				return 0;
			}
		}

		/* Controllo se il nodo da eliminare ha solo il figlio sinistro.
		*/
		else if (corr->right==NULL && corr->left!=NULL)
		{
			/* Guardo se "corr" è a destra o a sinistra del padre, memorizzato nella variabile "padre",
				 in modo da far diventare il figlio sinistro di "corr", figlio destro o sinistro di "padre".
				 Successivamente libero la memoria realitva al nodo da eliminare.
			*/
			if (strcmp (padre->user->name, puser->name)<0)
			{
				padre->right=corr->left;
				free (corr->user);
				free (corr);
				return 0;
			}
			else
			{
				padre->left=corr->left;
				free (corr->user);
				free (corr);
				return 0;
			}
		}

		/* Il nodo da eliminare ha entrambi i figli.
			 Cerco nel sottoalbero destro un nodo "corr_n" da mettere al posto di "corr".
		*/
		else
		{
			/* Cerco nel sottoalbero destro il nodo più a sinistra.
			*/
			corr_n= corr->right; padre_n= corr;
			while (corr_n->left!=NULL)
				{ padre_n=corr_n; corr_n=corr_n->left; }

			/* Controllo se "corr_n" è una foglia.
			*/
			if (corr_n->right==NULL)
			{
				/* Controllo che "corr_n" non sia il figlio sinistro di "corr".
					 Se non è il figlio di "corr_n" allora il figlio sinistro di "corr"
					 diventa il figlio sinistro di "corr_n", altrimenti il figlio sinistro di "corr_n"
					 è uguale a NULL.
				*/
				if (corr_n!=corr->left)
					corr_n->left=corr->left;
				else
					corr_n->left=NULL;

				/* Controllo che "corr_n" non sia il figlio destro di "corr".
					 Se non è il figlio di "corr_n" allora il figlio destro di "corr"
					 diventa il figlio destro di "corr_n", altrimenti il figlio destro di "corr_n"
					 è uguale a NULL.
				*/
				if (corr_n!=corr->right)
					corr_n->right=corr->right;
				else
					corr_n->right=NULL;

				/* Controllo se "corr" è il figlio destro o sinistro del padre e
					 sostituisco "corr" con "corr_n", libero la memoria relativa a "corr"
					 e restituisco 0.
				*/
				if ((strcmp(padre->user->name, puser->name))<0)
					padre->right=corr_n;
				else
					padre->left=corr_n;
				free (corr->user);
				free (corr);
				return 0;
			}

			/* Il nodo "corr_n" ha un figlio destro.
			*/
			else
			{
				/* Il sottoalbero destro di "corr_n" diventa il sottoalbero sinistro del padre di "corr_n".
				*/
				padre_n->left=corr_n->right;

				/* Controllo che "corr_n" non sia il figlio sinistro di "corr".
					 Se non è il figlio di "corr_n" allora il figlio sinistro di "corr"
					 diventa il figlio sinistro di "corr_n", altrimenti il figlio sinistro di "corr_n"
					 è uguale a NULL.
				*/
				if (corr_n!=corr->left)
					corr_n->left=corr->left;
				else
					corr_n->left=NULL;

				if (corr_n!=corr->right)
					corr_n->right=corr->right;
				else
					corr_n->right=NULL;

				
				/* Controllo se "corr" è il figlio destro o sinistro del padre e
					 sostituisco "corr" con "corr_n", libero la memoria relativa a "corr"
					 e restituisco 0.
				*/
				if ((strcmp(padre->user->name, puser->name))<0)
					padre->right=corr_n;
				else
					padre->left=corr_n;
				free (corr->user);
				free (corr);
				return 0;
				
			}
		}
	}
	else
		return WRPWD;
	return -1;
}




/** \brief La funzione dealloca ricorsivamente l'albero degli utenti.
*/
void freeTree (nodo_t *r)
{
	if (r==NULL)
		return;

	freeTree (r->left);
	freeTree (r->right);
	free (r->user);
	free (r);
}




/** \brief La funzione inserisce gli utenti presenti nel file "fin", passato come parametro,
		nell'albero r, passato come parametro.

	* Dopo aver verificato la correttezza degli argomenti, leggo una riga dal file e inserisce
	* nell'albero l'utente convertito in struttura ::user_t con la funzione ::stringToUser.
*/
int loadUsers (FILE *fin, nodo_t **r)
{
	char *tmp=NULL;
	int n=0, len=0;

	/* Controllo che il file sia diverso da NULL. Altrimenti setto errno a EINVAL e restituisco -1.
	*/
	if (fin==NULL)
		return -1;

	/* La lunghezza massima della stringa che si può acquisire da file è la lunghezza
		 del nome utente più la lunghezza della password più tre caratteri per il carattere ':'
		 per il carattere '\n' e il carattere di fine stringa.
	*/
	len=LUSER+LPWD+3;
	if ((tmp= (char *) malloc (len*sizeof (char)))==NULL)
		return -1;

	/* Finché non arrivo alla fine del file acquisisco una stringa dal file,
		 elimino il carattere '\n' alla penultima posizione della stringa
		 sostituendolo con il carattere di fine stringa e la inserisco nell'albero
		 tramite la funzione stringToUser.
	*/
  while (!feof(fin))
	{
		if ((fgets (tmp, len, fin))==NULL)
		{
			free (tmp);
			return n;
		}
		tmp[strlen (tmp) -1]='\0';
		
		if (addUser (r, stringToUser (tmp, strlen(tmp)))==0)
		  n++;
	}

	free (tmp);
	return n;
}




/** \brief La funzione inserisce l'utente "us", passato come argomento, nel file "fout",
		passato come argomento nel formato "nome:password\n".

		\param us utente da inserire nel file
		\param fout file in cui inserire l'utente

		\retval 1 se l'inserimento è avvenuto correttamente
		\retval 0 se si è verificato un errore 
*/
static int Store (user_t *us, FILE *fout)
{
	if (us==NULL || fout==NULL)
		return 0;
	if ((fprintf (fout, "%s:%s\n", us->name, us->passwd))==((strlen (us->name))+(strlen(us->passwd))+2))
		return 1;
	else
		return 0;
}




/** \brief La funzione scrive sul file "fout", passato come parametro, gli utenti dell'albero
		"r", passato come parametro.

	* La funzione procede ricorsivamente, scandendo l'albero degli utenti e richiamando la funzione ::Store
	* per scrivere gli utenti su file, e restituisce il numero di utenti scritti.
*/
int storeUsers (FILE *fout, nodo_t *r)
{
	if (r==NULL)
		return 0;
	if (fout==NULL)
		return -1;

	return (storeUsers (fout, r->left))+(Store (r->user, fout))+(storeUsers (fout, r->right));
}




/** \brief La funzione fornisce lo stato di un utente "u", passato come parametro, se esiste.

		La funzione procede scandendo l'albero finché non trova l'utente cercato e restituisce
		il suo stato se è presente, ::NOTREG altrimenti.
*/
status_t getUserStatus (nodo_t *r, char *u)
{
	nodo_t *app=r;

	if (u==NULL)
		return NOTREG;

	/* Scansiono l'albero finché non arrivo in fondo o non trovo l'utente cercato.
	*/
	while (app!=NULL && (strcmp (app->user->name, u))!=0)
	{
		if ((strcmp (app->user->name, u))>0)
			app=app->left;
		else
			app=app->right;
	}

	if (app==NULL)
		return NOTREG;
	else
		return app->status;
}




/** \brief La funzione fornisce il canale di un utente "u", passato come parametro, se esiste.

		La funzione procede scandendo l'albero finché non trova l'utente cercato e restituisce
		il suo canale se è presente, ::NOTREG altrimenti.
*/
int getUserChannel (nodo_t *r, char *u)
{
	nodo_t *app=r;

	if (u==NULL)
		return NOTREG;

	/* Scansiono l'albero finché non arrivo fino in fondo o non trovo l'utente cercato.
	*/
	while (app!=NULL && (strcmp (app->user->name, u))!=0)
	{
		if ((strcmp (app->user->name, u))>0)
			app=app->left;
		else
			app=app->right;
	}

	if (app==NULL)
		return NOTREG;
	else
		return app->channel;
}



/** \brief La funzione setta lo stato di un utente "u", passato come parametro, modificandolo
		nello stato "s", passato come parametro.

	* La funzione procede scandendo l'albero fino a che non arriva fino in fondo o non trova
	* l'utente cercato e restituisce l'esito dell'operazione.
*/
bool_t setUserStatus (nodo_t *r, char *u, status_t s)
{
	if (u==NULL)
		return FALSE;

	/* Scansiono l'albero "r" finché non arrivo fino in fondo o non trovo l'utente cercato.
	*/
	while (r!=NULL && (strcmp (r->user->name, u))!=0)
	{
		if ((strcmp (r->user->name, u))>0)
			r=r->left;
		else
			r=r->right;
	}

	/* Se l'utente non esiste restituisco FALSE, altrimenti ne modifico lo stato e restituisco TRUE.
	*/
	if (r==NULL)
		return FALSE;
	else
		{
			r->status=s;
			return TRUE;
		}
}




/** \brief La funzione setta il canale di un utente "u", passato come parametro, modificandolo
		nel canale "ch", passato come parametro.

	* La funzione procede scandendo l'albero fino a che non arriva fino in fondo o non trova
	* l'utente cercato e restituisce l'esito dell'operazione.
*/
bool_t setUserChannel (nodo_t *r, char *u, int ch)
{
	if (u==NULL)
		return FALSE;

	/* Scansiono l'albero "r" finché non arrivo fino in fondo o non trovo l'utente cercato
	*/
	while (r!=NULL && (strcmp (r->user->name, u))!=0)
	{
		if ((strcmp (r->user->name, u))>0)
			r=r->left;
		else
			r=r->right;
	}

	/* Se l'utente non esiste restituisco FALSE, altrimenti ne modifico il canale e restituisco TRUE.
	*/
	if (r==NULL)
		return FALSE;
	else
	{
		r->channel=ch;
		return TRUE;
	}
}

/** \brief La funzione controlla se un utente "u", passato come parametro, è presente nell'albero.

	* La funzione scansiona l'albero finché non arriva fino in fondo o non trova l'utente cercato
	* e restituisce l'esito dell'operazione.
*/
bool_t isUser (nodo_t *r, char *u)
{
	/* Scansiono l'albero "r" finché non arrivo fino in fondo o finché non trovo l'utente cercato.
		 Restituisco FALSE se sono arrivato fino in fondo, TRUE altrimenti.
	*/
	while (r!=NULL && (strcmp (r->user->name, u))!=0)
	{
		if ((strcmp (r->user->name, u))>0)
			r=r->left;
		else
			r=r->right;
	}

	if (r==NULL)
		return FALSE;
	else
		return TRUE;
}




/** \brief La funzione inserisce ricorsivamente in ordine lessicografico gli utenti dell'albero "r", passato
		come parametro, con stato uguale ad "st", passato come parametro, nella stringa "ret",
		passata come parametro.

	* La funzione procede ricorsivamente scandendo l'albero degli utenti e per ogni utente cui lo stato
	* è uguale a st, re-alloca memoria per l'utente considerato e inserisce l'utente nella stringa.
	* La variabile "controllo", passata come parametro per indirizzo, serve per vedere che non ci siano
	* stati errori nelle chiamate precedenti della realloc.

	\param ret Stringa in cui copiare la lista degli utenti.
	\param r Albero degli utenti
	\param st Stato da ricercare 
	\param controllo Intero per memorizzare errori nelle chiamate della realloc.
*/
static void cercaUtenti (char **ret, nodo_t *r, status_t st, int *controllo)
{
	char *tmp;
	if (r==NULL)
		return ;

	cercaUtenti (ret, r->left, st, controllo);

	if (r->status==st)
	{
		/* Controllo se la stringa "ret" è uguale a NULL e la variabile "controllo" è uguale a 1,
			 ovvero se è la prima volta che considero un nodo con stato uguale a "st".
		*/
		if (*ret==NULL && *controllo==1)
		{
			if ((*ret= (char *) malloc ((LUSER+2)*sizeof(char)))==NULL)
				{
					*controllo=0;
					return ;
				}
			sprintf (*ret, "%s", r->user->name);
		}

		else
		{
			/* Controllo che la realloc vada bene. Se non va bene libero la memoria già allocata
				 per la stringa "ret", setto la variabile "controllo" a 0 per significare che la
				 funzione è andata male per la prima volta, setto errno a ENOMEM e termino la funzione.
			*/
			if ((tmp= (char *) realloc (*ret, (strlen(*ret)+LUSER+1)*sizeof(char)))==NULL)
				{
					free (*ret);
					*ret=NULL;
					*controllo=0;
					errno= ENOMEM;
					return ;
				}
			*ret=tmp;
			sprintf (*ret, "%s:%s", *ret, r->user->name);
		}
	}

	cercaUtenti (ret, r->right, st, controllo);
}




/** \brief La funzione restituisce la lista degli utenti dell'albero "r", passato come parametro,
		con stato uguale a "st", passato come parametro.

		La funzione richiama la procedura ::cercaUtenti e restituisce la stringa generata dalla procedura
*/
char *getUserList (nodo_t *r, status_t st)
{
	char *ret=NULL;
	int controllo=1;

	cercaUtenti (&ret, r, st, &controllo);
	
	return ret;
}
