/**
   \file comsock.c
   \author Luca Di Mauro.
   \brief Implementazione delle funzioni definite in comsock.h.

		Si dichiara che il contenuto di questo file e' in ogni sua parte opera
		originale dell' autore.
 */
#include "comsock.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/un.h>


/** \brief La funzione crea una socket AF_UNIX con nome "path" pasato come parametro.

	* Dopo aver controllato la correttezza dei parametri, creo la socket, le assegno un nome,
	* e mi metto in ascolto per nuove connessioni.
*/
int createServerChannel (char* path)
{
	/* Struttura per l'indirizzo della socket. */
	struct sockaddr_un sa;
	/* File descriptor della socket. */
	int fd_skt=0;


	/* Controllo che il parametro passato in ingresso sia esatto.
	*/
	if (path==NULL)
	{
		errno= EINVAL;
		return -1;
	}
	if ((strlen (path))>UNIX_PATH_MAX || (strlen (path))==0)
	{
		errno= E2BIG;
		return -1;
	}


	/* Inizializzo la struttura che contiene l'indirizzo
			della socket per accettare le connessioni.
	*/
	strncpy (sa.sun_path, path, UNIX_PATH_MAX);
	sa.sun_family= AF_UNIX;

	/* Creo la socket, le assegno un nome e mi metto in ascolto per nuove connessioni.
	*/
	if ((fd_skt= socket (AF_UNIX, SOCK_STREAM, 0))==-1)
		return -1;

	/* Se non riesco a dare un nome alla socket, chiudo il descrittore e la rimuovo.
	*/
	if ((bind (fd_skt, (struct sockaddr *) &sa, sizeof (sa)))==-1)
	{
		if ((close (fd_skt))==-1)
			return -1;

		if ((remove (path))==-1)
			return -1;

		return -1;
	}
	
	/* Se non riesco a mettermi in attesa per nuove connessioni,
		 chiudo il descrittore e rimuovo la socket.
	*/
	if ((listen (fd_skt, SOMAXCONN))==-1)
	{
		if ((close (fd_skt))==-1)
			return -1;

		if ((remove (path))==-1)
			return -1;

		return -1;
	}

	return fd_skt;
}



/**	\brief La funzione chiude un canale lato server (rimuovendo la socket dal file system).

	* Dopo aver controllato la correttezza dei parametri, chiudo la socket e la rimuovo dal file system.
*/
int closeServerChannel(char* path, int s)
{
	/* Controllo che i parametri passati in ingresso siano esatti
	*/
	if (path==NULL || s<0)
	{
		errno= EINVAL;
		return -1;
	}

	/* Chiudo la socket e la rimuovo dal file system
	*/
	if ((close (s))==-1)
		return -1;

	if ((remove (path))==-1)
		return -1;

	return 0;
}



/** \brief La funzione accetta una connessione da parte di un client.

	* Dopo aver controllato la correttezza dei parametri, restituisco direttamente
	* il valore dell'ultima funzione (accept), poiché restituisco il valore che essa
	* mi restituisce sia in caso di errore che non.
*/
int acceptConnection(int s)
{
	if (s<0)
	{
		errno=EINVAL;
		return -1;
	}

	return accept (s, NULL, 0);
}



/** \brief La funzione scrive un messaggio sulla socket.

	* Sulla stringa viene scritto prima la lunghezza del campo buffer, il carattere per il tipo
	* e infine il campo buffer del messaggio. Le scritture sulla stringa da inviare vengono
	* fatte con la memcpy.
*/
int sendMessage(int sc, message_t *msg)
{
	/* Numero dei caratteri da allocare e scrivere. */
	int n_byte=0;
	/* Caratteri scritti. */
	int n=0;
	/* Stringa da inviare. */
	char *s_send=NULL;
	
	/* Controllo che i parametri passati siano bene formattati.
		 Prima controllo se msg esiste poi che il campo val e il campo buffer corrispondano.
	*/
	if (msg==NULL || sc<0)
	{
		errno=EINVAL;
		return -1;
	}

	if ((msg->length!=0 && msg->buffer==NULL) || (msg->length==0 && msg->buffer!=NULL))
	{
		errno=EINVAL;
		return -1;
	}
	
	/*	Se esiste il campo buffer, calcolo la lunghezza del messaggio da inviare pari
			alla lunghezza del messaggio (msg->length) più 1 byte per il tipo più 4 byte
			per il numero che rappresenta la lunghezza.
	*/
	if (msg->length!=0)
	{
		n_byte= (msg->length) + sizeof (char) + sizeof (int);
		if ((s_send=malloc (n_byte * sizeof(char)))==NULL)
			return -1;
		
		memcpy (s_send, &msg->length, sizeof (int));
		memcpy (s_send+sizeof(int), &msg->type, sizeof (char));
		memcpy (s_send+sizeof(int)+sizeof(char), msg->buffer, (sizeof (char) * msg->length));
	}

	/*	Se il campo buffer non esiste, la lunghezza del messaggio è pari a 5 byte,
			uno per il carattere e 4 per l'intero.
	*/
	else
	{
		n_byte= sizeof (char) + sizeof (int);
		if ((s_send=malloc (n_byte * sizeof(char)))==NULL)
			return -1;
		
		memcpy (s_send, &msg->length, sizeof (int));
		memcpy (s_send+sizeof(int), &msg->type, sizeof (char));
	}


	/* Scrivo il messaggio con una write nella socket con fd sc. */
	if ((n=write (sc, s_send, n_byte))==-1)
	{
		free (s_send);
		errno=ENOTCONN;
		return -1;
	}
	if (n<n_byte)
	{
		free (s_send);
		return -1;
	}

	free (s_send);

	return n;
}



/**	\brief La funzione legge un messaggio dalla socket.

	* Dopo aver controllato la correttezza dei parametri leggo l'intero e il carattere
	* dalla socket, rispettivamente la lunghezza del buffer e il tipo del messaggio,
	* e dopo leggo il messaggio, inserendo tutto nella struttura ::message_t passata
	* come parametro.
*/

int receiveMessage(int sc, message_t *msg)
{
	/* Caratteri letti dalle varie read. */
	int n=0;
	/* Caratteri restituiti. */
	int readed=0;

	/* Controllo che i parametri passati siano ben formattati, ovvero che msg->buffer
		 sia diverso da NULL.
	*/
	if (msg==NULL || sc<0)
	{
		errno=EINVAL;
		return -1;
	}

	/* Leggo l'intero e il carattere dalla socket.
	*/
	if((n=read (sc, &msg->length, sizeof (int)))==-1)
	{
		errno=ENOTCONN;
		return -1;
	}
	readed+=n;

	if((n=read (sc, &msg->type, sizeof (char)))==-1)
	{
		errno=ENOTCONN;
		return -1;
	}
	readed+=n;

	/* Alloco memoria e leggo il messaggio vero e proprio lungo "msg->length" byte.
	*/
	if (msg->length!=0)
	{
		if ((msg->buffer= malloc ((msg->length+1)*sizeof (char)))==NULL)
		{
			return -1;
		}
		if ((n=read (sc, msg->buffer, msg->length))==-1)
		{
			errno=ENOTCONN;
			return -1;
		}
		msg->buffer [msg->length]='\0';
		readed+=n;
	}
	else
	{
		msg->buffer=NULL;
	}
	if (n<msg->length)
	{
		free (msg->buffer);
		errno= EBADMSG;
		return -1;
	}

	return readed;
}



/** \brief La funzione crea una connessione alla socket del server.
*/
int openConnection(char* path, int ntrial, int k)
{
	/* Indirizzo della socket. */
	struct sockaddr_un sa;
	/* File descriptor della socket. */
	int FD=-1;


	/* Controllo che i parametri siano esatti. */
	if (path==NULL || ntrial>MAXTRIAL || ntrial<0 || k>MAXSEC || k<0)
	{
		errno=EINVAL;
		return -1;
	}
	if ((strlen (path))>UNIX_PATH_MAX)
	{
		errno=E2BIG;
		return -1;
	}

	/* Iniziaizzo la struttura che contiene l'inidirzzo della socket per poter connettermi
		 e creo la socket.
	*/
	strncpy (sa.sun_path, path, UNIX_PATH_MAX);
	sa.sun_family= AF_UNIX;
	FD= socket (AF_UNIX, SOCK_STREAM, 0);

	while ((ntrial) && (connect (FD, (struct sockaddr *) &sa, sizeof (sa))==-1))
	{
		if (errno==ENOENT)
		{
			sleep (k);
			ntrial--;
		}
		else
		{
			close (FD);
			remove (path);
		}
	}

	return FD;
}




/**	\brief La funzione chiude una connessione.
*/
int closeConnection (int s)
{
	return close (s);
}
