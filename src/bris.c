/** \file bris.c
		\author Luca Di Mauro.
		\brief Implementazione delle funzioni definite in bris.h.
		
		Si dichiara che il contenuto di questo file e' in ogni sua parte opera
		originale dell' autore.
*/
#include "bris.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

/** \brief La funzione stampa la carta "c" passata come parametro nel file "fpd" passato come parametro.
*/
void printCard (FILE *fpd, carta_t *c)
{
	/* Se uno dei parametri è uguale a NULL termina.
	*/
	if  (fpd==NULL || c==NULL )
			return ;

	/* Stampo sul file il carattere corrispondente al valore della carta.
	*/
	switch (c->val)
	{
		case ASSO:
			fprintf (fpd, "%c", 'A');
			break ;
		case DUE:
			fprintf (fpd, "%c", '2');
			break ;
		case TRE:
			fprintf (fpd, "%c", '3');
			break ;
		case QUATTRO:
			fprintf (fpd, "%c", '4');
			break ;
		case CINQUE:
			fprintf (fpd, "%c", '5');
			break ;
		case SEI:
			fprintf (fpd, "%c", '6');
			break ;
		case SETTE:
			fprintf (fpd, "%c", '7');
			break ;
		case FANTE:
			fprintf (fpd, "%c", 'J');
			break ;
		case DONNA:
			fprintf (fpd, "%c", 'Q');
			break ;
		case RE:
		  fprintf (fpd, "%c", 'K');
		  break ;
	}

	/* Stampo su file il carattere corrispondente al seme della carta.
	*/
	switch (c->seme)
	{
		case CUORI:
			fprintf (fpd, "%c", 'C');
			break ;
		case QUADRI:
			fprintf (fpd, "%c", 'Q');
			break ;
		case FIORI:
			fprintf (fpd, "%c", 'F');
			break ;
		case PICCHE:
			fprintf (fpd, "%c", 'P');
			break ;
	}
}

/** \brief La funzione converte la carta "c", passata come paramtro, in una stringa.
*/
void cardToString (char* s, carta_t *c)
{
	/* Se uno dei parametri è uguale a NULL ritorna NULL.
	*/
	if (c==NULL || s==NULL)
		return ;

	/* Scrivo nel primo carattere della stringa (s[0]) il carattere corrispondente al valore della carta.
	*/
	switch (c->val)
	{
		case ASSO:
			s[0]= 'A';
			break ;
		case DUE:
			s[0]='2';
			break ;
		case TRE:
			s[0]='3';
			break ;
		case QUATTRO:
			s[0]='4';
			break ;
		case CINQUE:
			s[0]='5';
			break ;
		case SEI:
			s[0]='6';
			break ;
		case SETTE:
			s[0]='7';
			break ;
		case FANTE:
			s[0]='J';
			break ;
		case DONNA:
			s[0]='Q';
			break ;
		case RE:
			s[0]='K';
			break ;
	}
	
	/* Scrivo nel secondo carattere della stringa (s[1]) il carattere corrispondente al seme della carta.
	*/
	switch (c->seme)
	{
		case CUORI:
			s[1]='C';
			break ;
		case QUADRI:
			s[1]='Q';
			break ;
		case FIORI:
			s[1]='F';
			break ;
		case PICCHE:
			s[1]='P';
			break ;
	}
	
	/* Scrivo nel terzo carattere della stringa (s[2]) il carattere di fine stringa.
	*/
	s[2]='\0';
	
}

/** \brief La funzione converte la stringa "str" passata come parametro in una struttura ::carta_t allocando memoria per la carta.
*/
carta_t* stringToCard (char* str)
{
	carta_t *new=NULL;

	/* Controllo che la stringa contenga almeno due elementi.
	*/
	if (strlen (str)<2)
		return NULL;

	if ((new= (carta_t *) malloc (sizeof(carta_t)))==NULL)
		return NULL;

	/* Registro il primo carattere della stringa (che è il valore) nel campo val della carta.
	*/
	switch (str[0])
	{
		case 'A':
			new->val=ASSO;
			break ;
		case '2':
			new->val=DUE;
			break ;
		case '3':
			new->val=TRE;
			break ;
		case '4':
			new->val=QUATTRO;
			break ;
		case '5':
			new->val=CINQUE;
			break ;
		case '6':
			new->val=SEI;
			break ;
		case '7':
			new->val=SETTE;
			break ;
		case 'J':
			new->val=FANTE;
			break ;
		case 'Q':
			new->val=DONNA;
			break ;
		case 'K':
			new->val=RE;
			break ;
	}

	/* Registro il secondo carattere della stringa (che è il seme) nel campo seme della carta.
	*/
	switch (str[1])
	{
		case 'C':
			new->seme=0;
			break ;
		case 'Q':
			new->seme=1;
			break ;
		case 'F':
			new->seme=2;
			break ;
		case 'P':
			new->seme=3;
			break ;
	}

	return new;
}

/** \brief La funzione pesca una carta dal mazzo e restituisce il puntatore alla carta allocando memoria.
*/
carta_t* getCard (mazzo_t* m)
{
	carta_t* new=NULL;

	/* Controllo se sono state pescate tutte le carte dal mazzo.
		 Se è vero, setto erno a 0 e restituisco NULL.
	*/
	if (m->next>=NCARTE)
		{
		  errno=0;
		  return NULL;
		}

	if ((new= (carta_t *) malloc (sizeof(carta_t)))==NULL)
		return NULL;

	/* Assegno il valore e il seme della carta pescata alla carta da restituire
		 e incremento il campo "next".
	*/
	new->val=m->carte[m->next].val;
	new->seme=m->carte[m->next].seme;
	m->next=m->next+1;

	return new;
}

/** \brief La funzione stampa il mazzo passato come parametro sul file passato come parametro.

	* Uso le variabili seme e val per memorizzarci, rispettivamente,
	* i campi "seme" e "val" dell'i-esima carta del mazzo.
*/
void printMazzo (FILE *fpd, mazzo_t *pm)
{
	int i=0;
	char seme, val;

	/* Se il mazzo è NULL termino la funzione.
	*/
	if (pm==NULL)
			return ;

	for (i=0;i<NCARTE;i++)
	  {
			/* Memorizzo nella variabile val il carattere corrispondente al valore della carta.
			*/
			switch (pm->carte [i].val)
			{
				case ASSO:
					val='A';
					break ;
				case DUE:
					val='2';
					break ;
				case TRE:
					val='3';
					break ;
				case QUATTRO:
					val='4';
					break ;
				case CINQUE:
					val='5';
					break ;
				case SEI:
					val='6';
					break ;
				case SETTE:
					val='7';
					break ;
				case FANTE:
					val='J';
					break ;
				case DONNA:
					val='Q';
					break ;
				case RE:
					val='K';
					break ;
			}

			/* Memorizzo nella variabile seme il carattere corrispondente al seme della carta.
			*/
			switch (pm->carte[i].seme)
			{
				case CUORI:
					seme='C';
					break ;
				case QUADRI:
					seme='Q';
					break ;
				case FIORI:
					seme='F';
					break ;
				case PICCHE:
					seme='P';
					break ;
			}

			/* Stampo su file il valore e il seme della carta
			*/
			fprintf (fpd, "%c%c\n", val, seme);
		}
}


/** \brief La funzione dealloca un mazzo di carte ::mazzo_t.
*/
void freeMazzo (mazzo_t *pm)
{
	free (pm);
	return ;
}

/** \brief La funzione compareVal determina la carta vincente tra due carte aventi lo stesso seme.

		\param ca Struttura ::carta_t da confrontare
		\param cb Struttura ::carta_t da confrontare

		\retval TRUE Se la carta ca batte la carta cb
		\retval FALSE Altrimenti

	* Per determinare la carta vincente considero il valore della carta "ca" e guardo se il valore della carta "cb"
	* è tale da far vincere la carta "ca" e restituisco TRUE, altrimenti restituisco FALSE.
*/
static bool_t compareVal (carta_t *ca, carta_t *cb)
{
	if (ca==NULL || cb==NULL)
		return FALSE;

	switch (ca->val)
	{
		case ASSO:
			return TRUE;

		case TRE:
			{
				if (cb->val!=ASSO)
					return TRUE;
				else
					return FALSE;
			}
			break;

		case RE:
			{
				if (cb->val!=ASSO && cb->val!=TRE)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case DONNA:
			{
				if (cb->val!=ASSO && cb->val!=TRE && cb->val!=RE)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case FANTE:
			{
				if (cb->val!=ASSO && cb->val!=TRE && cb->val!=RE && cb->val!=DONNA)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case SETTE:
			{
				if (cb->val!=ASSO && cb->val!=TRE && cb->val!=RE && cb->val!=DONNA && cb->val!=FANTE)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case SEI:
			{
				if (cb->val!=ASSO && cb->val!=TRE && cb->val!=RE && cb->val!=DONNA && cb->val!=FANTE && cb->val!=SETTE)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case CINQUE:
			{
				if (cb->val!=ASSO && cb->val!=TRE && cb->val!=RE && cb->val!=DONNA && cb->val!=FANTE && cb->val!=SETTE && cb->val!=SEI)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case QUATTRO:
			{
				if (cb->val!=ASSO && cb->val!=TRE && cb->val!=RE && cb->val!=DONNA && cb->val!=FANTE && cb->val!=SETTE && cb->val!=SEI && cb->val!=CINQUE)
					return TRUE;
				else
					return FALSE;
			}
			break ;

		case DUE:
			return FALSE;
			break ;
	}
	return TRUE;
}

/** \brief La funzione determina se la carta "ca", passata come parametro, batte la carta "cb",
		pasata come parametro, restituendo TRUE, altrimenti restituisce FALSE.

	* Uso la funzione ::compareVal per determinare la carta vincente
	* nel caso in cui le due carte abbiano seme uguale.
*/
bool_t compareCard (semi_t briscola, carta_t* ca, carta_t* cb)
{
	if (ca==NULL || cb==NULL)
		return FALSE;
	if (ca->seme==briscola && cb->seme==briscola)
		return compareVal (ca, cb);
	if (ca->seme==briscola && cb->seme!=briscola)
		return TRUE;
	if (ca->seme!=briscola && cb->seme==briscola)
		return FALSE;
	if (ca->seme==cb->seme)
		return compareVal (ca, cb);
	return TRUE;
}

/** \brief La funzione calcola e restituisce l'ammontare dei punti
		data una sequenza "c" di "n" carte, passata come parametro.
*/
int computePoints (carta_t**c, int n)
{
	int punti=0, i=0;
	for (i=0;i<n;i++)
	{
		if (c[i]->val==ASSO)
			punti+=11;
		else if (c[i]->val==TRE)
			punti+=10;
		else if (c[i]->val==RE)
			punti+=4;
		else if (c[i]->val==DONNA)
			punti+=3;
		else if (c[i]->val==FANTE)
			punti+=2;
	}

	return punti;
}
