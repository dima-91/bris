/**  \file 
     \brief generatore mazzi thread safe
     \author lso13 
*/

#ifndef __NEWMAZZO_R__H
#define __NEWMAZZO_R__H

/** versione threadsafe di newMazzo
    genera un mazzo di carte mischiato
    NON richiede che la srand() sia stata gia' invocata
    \param test TRUE se stiamo eseguendo in modalita' test 
                FALSE altrimenti 
    (serve per riprodurre sempre la stessa sequenza in modalita' test )

    ATTENZIONE: questa funzione e' fornita gia' implementata dai docenti 
    nel modulo oggetto newMazzo_r.o da inserire nella libreria libbris.a

    \retval p puntatore al nuovo mazzo 
    \retval NULL se si e' verificato un errore (setta errno)
 */
mazzo_t* newMazzo_r (bool_t test);
#endif
